﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Management;

namespace ControlDeIngresoVehicular
{
    public partial class Activacion : Form
    {
        BD bd = new BD();
        DataSet obtenerDatos;
        int serial = 0;
        string hexadecimal = "";

        public Activacion()
        {
            InitializeComponent();
            hexadecimal = codHex();
            serial = int.Parse(hexadecimal, System.Globalization.NumberStyles.HexNumber);
            serial = Math.Abs(serial);
        }

        public string codHex()
        {
            string HDD = System.Environment.CurrentDirectory.Substring(0, 1);

            ManagementObject disk = new ManagementObject("win32_logicaldisk.deviceid=\"" + HDD + ":\"");

            disk.Get();

            return disk["VolumeSerialNumber"].ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int serialActivacion = Convert.ToInt32(txtSerialActivacion.Text);

            if (serialActivacion == serial)
            {
                altaSerial(serial);
                this.Close();
                Application.Restart();
            }
            else 
            {
                MessageBox.Show("El numero es invalido favor de verificar");
            }
        }

        private void altaSerial(int serial) 
        {
            bool respuesta = bd.insertar("UPDATE `Activacion` SET `numeroActivacion`='" + serial + "' ,`status`= 1  WHERE `status`= 0");

            if (respuesta)
            {
                MessageBox.Show("Activacion del producto ralizada con EXITO!!");
            }
        }
    }
}

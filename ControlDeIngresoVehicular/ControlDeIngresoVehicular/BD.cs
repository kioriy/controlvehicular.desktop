﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;
using System.IO;
using System.Data.SQLite;

namespace ControlDeIngresoVehicular
{
    class BD
    {
        string parametrosConexion = System.Configuration.ConfigurationManager.ConnectionStrings["db"].ConnectionString;
        SQLiteConnection conexionBD;
        SQLiteCommand queryCommand;
        public SQLiteDataAdapter adaptador;
        SQLiteDataReader reader;
        SQLiteParameter picture;
        SQLiteParameter picture2;
        DataSet obtenerDatos;
        
        public bool conexionAbrir()
        {
            conexionBD = new SQLiteConnection(parametrosConexion);
            ConnectionState estadoConexion;
            try
            {
                conexionBD.Open();
                estadoConexion = conexionBD.State;
                return true;
            }
            catch(SQLiteException)
            {
                return false;
            }
        }

        public bool insertar(string sqliteInsertar) 
        {
            int filasAfectadas = 0;
            conexionAbrir();

            queryCommand = new SQLiteCommand(sqliteInsertar, conexionBD);
            filasAfectadas = queryCommand.ExecuteNonQuery();
            
            conexionCerrar();
            
            if (filasAfectadas > 0) 
            {
                return true;
            }
            else 
            {
                return false;
            }
        }

        public SQLiteDataAdapter consultaAdaptador(string queryConsulta) 
        {
            conexionAbrir();

            adaptador = new SQLiteDataAdapter(queryConsulta, conexionBD);

            conexionCerrar();

            return adaptador;
        }

        public void llenarComboBox(System.Windows.Forms.ComboBox llenarCombo, string sqlLlenarComboBox, string columna, string tabla, /*string condicion, string orderBy, bool conCondicion,*/ref int entra) 
        {
            //string sqlLlenarComboBox = "";

            //if (conCondicion)
            //{
            //    sqlLlenarComboBox  = "SELECT " + columna + " FROM " + tabla + " WHERE " + condicion + " ORDER BY " + orderBy + " ASC";
            //}
            //else
            //{
            //    sqlLlenarComboBox = "SELECT " + columna + " FROM " + tabla + " ORDER BY " + orderBy + " ASC";
            //}

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();
            adaptador = consultaAdaptador(sqlLlenarComboBox);

            adaptador.Fill(obtenerDatos,tabla);

            entra = 1;
            llenarCombo.DataSource = obtenerDatos.Tables[tabla];
            llenarCombo.DisplayMember = columna;
            llenarCombo.SelectedIndex = -1;
            entra = 0;
        }

        public void llenarListBox(System.Windows.Forms.ListBox llenarlista, string columna, string tabla, string condicion, string orderBy, bool conCondicion, ref int entra)
        {
            string sqlLlenarComboBox = "";

            if (conCondicion)
            {
                sqlLlenarComboBox = "SELECT " + columna + " FROM " + tabla + " WHERE " + condicion + " ORDER BY " + orderBy + " ASC";
            }
            else
            {
                sqlLlenarComboBox = "SELECT " + columna + " FROM " + tabla + " ORDER BY " + orderBy + " ASC";
            }

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();
            adaptador = consultaAdaptador(sqlLlenarComboBox);

            adaptador.Fill(obtenerDatos, tabla);

            entra = 1;
            llenarlista.DataSource = obtenerDatos.Tables[tabla];
            llenarlista.DisplayMember = columna;
            //llenarCombo.SelectedIndex = -1;
            entra = 0;
        }

        private void conexionCerrar()
        {
           if (conexionBD.State == ConnectionState.Open)
           {
               conexionBD.Close();
           }
        }

        public bool guradarImagen(System.Windows.Forms.PictureBox pbImagen, System.Windows.Forms.PictureBox pbImagen2, int id) 
        {
            if (pbImagen.Image != null) 
            {
                int filasAfectadas = 0;

                picture = new SQLiteParameter("@picture", SqlDbType.Image);
                picture2 = new SQLiteParameter("@picture2", SqlDbType.Image);

                MemoryStream ms = new MemoryStream();
                MemoryStream ms2 = new MemoryStream();

                pbImagen.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                pbImagen2.Image.Save(ms2, System.Drawing.Imaging.ImageFormat.Bmp);
                

                byte[] imagenByte = ms.GetBuffer();
                byte[] imagenByte2 = ms2.GetBuffer();

                ms.Close();
                conexionAbrir();
                queryCommand = new SQLiteCommand(conexionBD);
                queryCommand.Parameters.Clear();
                queryCommand.Parameters.AddWithValue("@picture", imagenByte);
                queryCommand.Parameters.AddWithValue("@picture2", imagenByte2);
                queryCommand.CommandText = /*"UPDATE `Visitante` SET `fotorostro` = (@picture) WHERE `idvisitante` = " + idVisitante + "";*/"INSERT INTO `Imagen`(`idVisitante`,`fotoRostro`,`fotoCredencial`) VALUES (" + id + ", @picture, @picture2)";

                filasAfectadas =  queryCommand.ExecuteNonQuery();
                conexionCerrar();

                if (filasAfectadas > 0) 
                {
                    return true;
                }
                else 
                {
                    return false;
                }
            }
            return false;
        }

        public bool actualizar(string tabla, string datos, string condicion) 
        {
            conexionAbrir();

            int filasAfectadas = 0;
            string queryActualizar = "UPDATE " + tabla + " SET " + datos + " WHERE " + condicion;

            queryCommand = new SQLiteCommand(queryActualizar, conexionBD);

            filasAfectadas = queryCommand.ExecuteNonQuery();

            conexionCerrar();

           if (filasAfectadas > 0)
           {
               return true;
           }
           else
           {
               return false;
           }
        }

        public void llenarDgv(System.Windows.Forms.DataGridView dgvLlenar, string columnas, string tabla, string condicion, string orderBy) 
        {
            obtenerDatos = new DataSet();
            obtenerDatos.Reset();
        
            adaptador = consultaAdaptador("SELECT " + columnas + " FROM " + tabla + " WHERE " + condicion + " ORDER BY " + orderBy + " ASC");
            adaptador.Fill(obtenerDatos, tabla);

            dgvLlenar.DataSource = obtenerDatos.Tables[0];
        }
    }
}

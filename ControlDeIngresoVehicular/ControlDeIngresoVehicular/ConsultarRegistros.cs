﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace ControlDeIngresoVehicular
{
    public partial class ConsultarRegistros : Form
    {
        BD bd = new BD();
        DataSet obtenerDatos;
        int entra = 0;

        public ConsultarRegistros()
        {
            InitializeComponent();
            string select = "v.nombre, r.numeroCasa, r.horaEntrada, r.horaSalida, r.fechaIngreso, r.fechaSalida, u.nombreUsuario ";
            string from = "Visitante AS v, Registro AS r, Usuario AS u";
            string condicion = "r.idVisitante = v.idvisitante AND u.idGuardia = r.idGuardia";
            //string select = "v.nombre as `Visitante`, r.numeroCasa, c.nombre as `Habitante visitado`, r.horaEntrada, r.horaSalida, r.fechaIngreso";
            //string from = "Visitante AS v, Registro AS r, Colono AS c";
            //string condicion = "v.idVisitante = r.idVisitante and c.idcolono = r.idColono";
            string queryCbGuardia = "SELECT `nombreUsuario` FROM `Usuario` ORDER BY `nombreUsuario` ASC";
            string queryCbVisitante = "SELECT DISTINCT `nombre` FROM `Visitante` ORDER BY `nombre` ASC";

            bd.llenarComboBox(cbNombreGuardia, queryCbGuardia, "nombreUsuario", "Usuario", ref entra);
            bd.llenarComboBox(cbVisitante, queryCbVisitante, "nombre", "`Visitante`", ref entra);
            bd.llenarDgv(dgvConsultaRegistro, select, from, condicion, "v.nombre");
        }

        private void cmdConsulta_Click(object sender, EventArgs e)
        {
            string select = "v.nombre, r.numeroCasa, r.horaEntrada, r.horaSalida, r.fechaIngreso, r.fechaSalida, u.nombreUsuario ";
            string from = "Visitante AS v, Registro AS r, Usuario AS u";
            string condicion = "r.idVisitante = v.idvisitante AND u.idGuardia = r.idGuardia";

            string fecha = dtpFecha.Value.ToShortDateString();
            string guardia = cbNombreGuardia.Text;
            string visitante = cbVisitante.Text;
            string condicionFecha = " AND r.fechaIngreso = '" + fecha + "'";
            string condicionGuardia = " AND u.nombreUsuario = '" + guardia + "'";
            string condicionVisitante = " AND v.nombre = '" + visitante + "'";

            if (chkConFecha.Checked)
            {
                condicion += condicionFecha;
            }

            if (cbNombreGuardia.SelectedIndex > -1 && guardia != "") 
            {
                condicion += condicionGuardia;
            }

            if (cbVisitante.SelectedIndex > -1 && visitante != "") 
            {
                condicion += condicionVisitante;
            }
            
            bd.llenarDgv(dgvConsultaRegistro, select, from, condicion, "v.nombre");

            cbNombreGuardia.SelectedIndex = -1;
            cbVisitante.SelectedIndex = -1;
        }

        private void cmdMostrarTodo_Click(object sender, EventArgs e)
        {
            string select = "v.nombre, r.numeroCasa, r.horaEntrada, r.horaSalida, r.fechaIngreso, r.fechaSalida, u.nombreUsuario ";
            string from = "Visitante AS v, Registro AS r, Usuario AS u";
            string condicion = "r.idVisitante = v.idvisitante AND u.idGuardia = r.idGuardia";

            bd.llenarDgv(dgvConsultaRegistro, select, from, condicion, "v.nombre");

            cbNombreGuardia.SelectedIndex = -1;
            cbVisitante.SelectedIndex = -1;
        }

        private void dgvConsultaRegistro_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //pbCapturaRostro = null;
            //pbCaturaCredencial = null;
            try
            {
                string nombreVisitante = dgvConsultaRegistro[0, e.RowIndex].Value.ToString();

                int id = idVisitante(nombreVisitante);

                cargarImagen(id);
            }
            catch (Exception) { }
        }
        
        public void cargarImagen(int idVisitante)
        {
            byte[] imagenRostro;
            byte[] imagenCredencial;

            string sqlConsulta = "SELECT `fotoRostro`,`fotoCredencial` FROM `Imagen` WHERE `idVisitante` = " + idVisitante + "";

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(sqlConsulta);
            bd.adaptador.Fill(obtenerDatos, "Imagen");

            imagenRostro = (byte[])obtenerDatos.Tables["Imagen"].Rows[0]["fotoRostro"];
            imagenCredencial = (byte[])obtenerDatos.Tables["Imagen"].Rows[0]["fotoCredencial"];

            MemoryStream msRostro = new MemoryStream(imagenRostro);
            MemoryStream msCredencial = new MemoryStream(imagenCredencial);

            //pbCapturaRostro.Visible = true;
            //pbCaturaCredencial.Visible = true;
            //pbImagenRostro.Visible = false;
            //pbImagenCredencial.Visible = false;

            pbImagenRostro.Image = Image.FromStream(msRostro);
            pbImagenCredencial.Image = Image.FromStream(msCredencial);
        }

        public int idVisitante(string nombreVisitante)
        {
            int id = 0;
            string sqlConsulta = "SELECT `idvisitante` FROM `Visitante`  WHERE `nombre` = '" + nombreVisitante + "'";

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(sqlConsulta);
            bd.adaptador.Fill(obtenerDatos, "Visitante");

            try
            {
                string recibeDato = obtenerDatos.Tables[0].Rows[0][0].ToString();
                id = Convert.ToInt32(recibeDato);
                return id;
            }
            catch (IndexOutOfRangeException)
            {
                return id;
            }
        }

       /* private void ExportarDataGridViewExcel(DataGridView dgv)
        {
            SaveFileDialog fichero = new SaveFileDialog();
            fichero.Filter = "Excel (*.xls)|*.xls";

            if (fichero.ShowDialog() == DialogResult.OK)
            {
                Microsoft.Office.Interop.Excel.Application aplicacion;
                Microsoft.Office.Interop.Excel.Workbook libros_trabajo;
                Microsoft.Office.Interop.Excel.Worksheet hoja_trabajo;

                aplicacion = new Microsoft.Office.Interop.Excel.Application();
                libros_trabajo = aplicacion.Workbooks.Add();
                hoja_trabajo = (Microsoft.Office.Interop.Excel.Worksheet)libros_trabajo.Worksheets.get_Item(1);


                //Recorremos el DataGridView rellenando la hoja de trabajo
                for (int i = 0; i < dgv.Rows.Count - 1; i++)
                {
                    //hoja_trabajo.Cells[1, i + 1] = dgv.Columns[i].Name.ToString();

                    for (int j = 0; j < dgv.Columns.Count; j++)
                    {
                        hoja_trabajo.Cells[1, j + 1] = dgv.Columns[j].Name.ToString();

                        if (dgv.Rows[i].Cells[j].Value == null)
                        {
                            hoja_trabajo.Cells[i + 2, j + 1] = "";
                        }
                        else
                        {
                            hoja_trabajo.Cells[i + 2, j + 1] = dgv.Rows[i].Cells[j].Value.ToString();
                        }
                    }
                }
                hoja_trabajo.Columns.AutoFit();
                hoja_trabajo.Rows.HorizontalAlignment = 3;
                libros_trabajo.SaveAs(fichero.FileName, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal);
                libros_trabajo.Close(true);
                aplicacion.Quit();
            }
        }

        private void cmdExcel_Click(object sender, EventArgs e)
        {
            ExportarDataGridViewExcel(dgvConsultaRegistro);
        }*/
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WIA;

namespace ControlDeIngresoVehicular
{
    class Escaner
    { 
        public DeviceInfo deviceInfo;
        //Scanner escaner;
        //public Scanner(DeviceInfo deviceInfo)
        //{
        //    this.deviceInfo = deviceInfo;
        //}

        public void cargarEscaner()
        {
            var deviceManager = new DeviceManager();

            deviceInfo = deviceManager.DeviceInfos[0];
        }

        public ImageFile Scan()
        {
            cargarEscaner();
            var device = deviceInfo.Connect();
            var item = device.Items[1];
            var imageFile = (ImageFile)item.Transfer("{B96B3CAE-0728-11D3-9D7B-0000F81EF32E}");
            return imageFile;
        }

        public override string ToString()
        {
            return this.deviceInfo.Properties["Name"].get_Value().ToString();  
        }
    }
}

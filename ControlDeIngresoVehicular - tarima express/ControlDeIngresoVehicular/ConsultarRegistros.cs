﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ControlDeIngresoVehicular
{
    public partial class ConsultarRegistros : Form
    {
        BD bd = new BD();
        DataSet obtenerDatos;
        int entra = 0;

        public ConsultarRegistros()
        {
            InitializeComponent();
            string select = "v.nombre AS Nombre, r.empresa AS Empresa, r.material AS Material, r.cantidad AS Cantidad, r.horaEntrada AS `Hora entrada`, r.horaSalida AS `Hora Salida`, r.fechaIngreso AS `Fecha Ingreso`, r.fechaSalida AS `Fecha salida`, u.nombreUsuario AS Usuario";
            string from = "Visitante AS v, Registro AS r, Usuario AS u";
            string condicion = "r.idVisitante = v.idvisitante AND u.idGuardia = r.idGuardia";
            //string select = "v.nombre as `Visitante`, r.numeroCasa, c.nombre as `Habitante visitado`, r.horaEntrada, r.horaSalida, r.fechaIngreso";
            //string from = "Visitante AS v, Registro AS r, Colono AS c";
            //string condicion = "v.idVisitante = r.idVisitante and c.idcolono = r.idColono";
            //string queryCbGuardia = "SELECT `nombreUsuario` FROM `Usuario` ORDER BY `nombreUsuario` ASC";
            string queryCbVisitante = "SELECT DISTINCT `nombre` FROM `Visitante` ORDER BY `nombre` ASC";
            string queryCbEmpresa = "SELECT DISTINCT `nombre` FROM `Empresa` ORDER BY `nombre` ASC";

            //bd.llenarComboBox(cbNombreGuardia, queryCbGuardia, "nombreUsuario", "Usuario", ref entra);
            bd.llenarComboBox(cbVisitante, queryCbVisitante, "nombre", "`Visitante`", ref entra);
            bd.llenarComboBox(cbEmpresa, queryCbEmpresa, "nombre", "`Empresa`", ref entra);
            bd.llenarDgv(dgvConsultaRegistro, select, from, condicion, "v.nombre");
        }

        private void cmdConsulta_Click(object sender, EventArgs e)
        {
            string select = "v.nombre AS Nombre, r.empresa AS Empresa, r.material AS Material, r.cantidad AS Cantidad, r.horaEntrada AS `Hora entrada`, r.horaSalida AS `Hora Salida`, r.fechaIngreso AS `Fecha Ingreso`, r.fechaSalida AS `Fecha salida`, u.nombreUsuario AS Usuario";
            string from = "Visitante AS v, Registro AS r, Usuario AS u";
            string condicion = "r.idVisitante = v.idvisitante AND u.idGuardia = r.idGuardia";

            string fecha = dtpFecha.Value.ToShortDateString();
            string material = cbMaterial.Text;
            string visitante = cbVisitante.Text;
            string empresa = cbEmpresa.Text;
            string condicionFecha = " AND r.fechaIngreso = '" + fecha + "'";
            string condicionMaterial = " AND r.material = '" + material + "'";
            string condicionVisitante = " AND v.nombre = '" + visitante + "'";
            string condicionEmpresa = " AND empresa = '" + empresa + "'";

            if (chkConFecha.Checked)
            {
                condicion += condicionFecha;
            }

            if (cbMaterial.SelectedIndex > -1 && material != "") 
            {
                condicion += condicionMaterial;
            }

            if (cbVisitante.SelectedIndex > -1 && visitante != "") 
            {
                condicion += condicionVisitante;
            }

            if (cbEmpresa.SelectedIndex > -1 && empresa != "")
            {
                condicion += condicionEmpresa;
            }
            
            bd.llenarDgv(dgvConsultaRegistro, select, from, condicion, "v.nombre");

            cbMaterial.SelectedIndex = -1;
            cbVisitante.SelectedIndex = -1;
            cbEmpresa.SelectedIndex = -1;
        }

        private void cmdMostrarTodo_Click(object sender, EventArgs e)
        {
            string select = "v.nombre AS Nombre, r.empresa AS Empresa, r.material AS Material, r.cantidad AS Cantidad, r.horaEntrada AS `Hora entrada`, r.horaSalida AS `Hora Salida`, r.fechaIngreso AS `Fecha Ingreso`, r.fechaSalida AS `Fecha salida`, u.nombreUsuario AS Usuario";
            string from = "Visitante AS v, Registro AS r, Usuario AS u";
            string condicion = "r.idVisitante = v.idvisitante AND u.idGuardia = r.idGuardia";

            bd.llenarDgv(dgvConsultaRegistro, select, from, condicion, "v.nombre");

            cbMaterial.SelectedIndex = -1;
            cbVisitante.SelectedIndex = -1;
        }

        private void dgvConsultaRegistro_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //pbCapturaRostro = null;
            //pbCaturaCredencial = null;
            try
            {
                string nombreVisitante = dgvConsultaRegistro[0, e.RowIndex].Value.ToString();

                int id = idVisitante(nombreVisitante);

                cargarImagen(id);
            }
            catch (Exception) { }
        }
        
        public void cargarImagen(int idVisitante)
        {
            byte[] imagenRostro;
            byte[] imagenCredencial;

            string sqlConsulta = "SELECT `fotoRostro`,`fotoCredencial` FROM `Imagen` WHERE `idVisitante` = " + idVisitante + "";

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(sqlConsulta);
            bd.adaptador.Fill(obtenerDatos, "Imagen");

            imagenRostro = (byte[])obtenerDatos.Tables["Imagen"].Rows[0]["fotoRostro"];
            imagenCredencial = (byte[])obtenerDatos.Tables["Imagen"].Rows[0]["fotoCredencial"];

            MemoryStream msRostro = new MemoryStream(imagenRostro);
            MemoryStream msCredencial = new MemoryStream(imagenCredencial);

            //pbCapturaRostro.Visible = true;
            //pbCaturaCredencial.Visible = true;
            //pbImagenRostro.Visible = false;
            //pbImagenCredencial.Visible = false;

            pbImagenRostro.Image = Image.FromStream(msRostro);
            pbImagenCredencial.Image = Image.FromStream(msCredencial);
        }

        public int idVisitante(string nombreVisitante)
        {
            int id = 0;
            string sqlConsulta = "SELECT `idvisitante` FROM `Visitante`  WHERE `nombre` = '" + nombreVisitante + "'";

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(sqlConsulta);
            bd.adaptador.Fill(obtenerDatos, "Visitante");

            try
            {
                string recibeDato = obtenerDatos.Tables[0].Rows[0][0].ToString();
                id = Convert.ToInt32(recibeDato);
                return id;
            }
            catch (IndexOutOfRangeException)
            {
                return id;
            }
        }
    }
}

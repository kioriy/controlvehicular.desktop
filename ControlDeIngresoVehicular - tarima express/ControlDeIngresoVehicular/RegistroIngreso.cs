﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using System.IO;
using System.Windows.Media.Imaging;

namespace ControlDeIngresoVehicular
{        
    public partial class frmRegistroIngreso : Form
    {
        Video camaraRostro;
        Video camaraCredencial;
        Escaner e = new Escaner();
        BD bd = new BD();
        DataSet obtenerDatos;
        string idGuardia = "";
        string permisos = "";
        string nombreUsuario = ""; 
        public int entra = 0;
        string sqlNumeroCasa = "SELECT `nombre` FROM `Empresa`  ORDER BY `nombre` ASC"; 

        //EVENTOS CARGA FORMULARIO
        public frmRegistroIngreso(string idGuardia, string permisos, string nombreUsuario)
        {
            InitializeComponent();

            this.idGuardia = idGuardia;
            this.permisos = permisos;
            this.nombreUsuario = nombreUsuario;

            if (permisos == "0") 
            {
                activarBotones();
            }
            else if (permisos == "1") 
            {
                desactivarBotones();
            }

            camaraRostro = new Video(pbImagenRostro, pbCapturaRostro);
            camaraCredencial = new Video(pbImagenCredencial, pbCaturaCredencial);
        }

        private void frmRegistroIngreso_Load(object sender, EventArgs e)
        {
            //camaraRostro.cargarVideo(0);
            //camaraCredencial.cargarVideo(1);
            camaraRostro.cargarComboBox(cbCamaraRostro);
            camaraCredencial.cargarComboBox(cbCamaraCredencial);
            tReloj.Start();
            lFecha.Text = DateTime.Today.ToString("dd - MMMM - yyyy").ToUpper();
            bd.llenarComboBox(cbCasaQueVisita, sqlNumeroCasa, "nombre", "Empresa", ref entra);
            tsCmdConfiguracion.Enabled = true;
            //bd.llenarComboBox(cbNombre, "nombre", "Visitante", "", "nombre", false, ref entra);
        }

        private void frmRegistroIngreso_FormClosed(object sender, FormClosedEventArgs e)
        {
            camaraRostro.detenerVideo();
            camaraCredencial.detenerVideo();
            tReloj.Stop();
            Application.Exit();
        }

        private void tReloj_Tick(object sender, EventArgs e)
        {
            lHora.Text = DateTime.Now.ToLongTimeString();
        }
        //FIN EVENTOS CARGA FORMULARIO
        
        //*
        
        //EVENTOS CAMBIO INDEX
        private void cbCasaQueVisita_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string numero = cbCasaQueVisita.Text;

            if (/*numero != "System.Data.DataRowView" && numero != ""*/entra != 1)
            {
                //cbRepresentante.DataSource = null;
                //bd.llenarListBox(lbHabitante, "nombre", "Colono", "numero = "+numero, "nombre", true,ref entra);

                ////cargar Telefono
                //obtenerDatos = new DataSet();
                //obtenerDatos.Reset();
                
                //bd.adaptador = bd.consultaAdaptador("SELECT telefono FROM Casa WHERE numero = '" + cbCasaQueVisita.Text + "'");
                //bd.adaptador.Fill(obtenerDatos, "Casa");

                //string recibeDato = obtenerDatos.Tables[0].Rows[0][0].ToString();

                //lTelefono.Text = Convert.ToString(recibeDato);

                //cbRepresentante.Enabled = true;
                lbHabitante.Enabled = true;
                pHabitante.BackColor = Color.LightGreen;
            }
        }

        private void cbNombreVisitante_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (entra != 1)
            {
                string statusMasIdVisitante = statusVisitante();
                string[] splitStatusMasIdVisitante = statusMasIdVisitante.Split(new char[] { ',' });
                int status = Convert.ToInt32(splitStatusMasIdVisitante[0]);
                string idVisitante = splitStatusMasIdVisitante[1];

                if (status == 1)
                {
                    //cmdRegistrarIngreso.Visible = false;
                    //cmdRegistrarSalida.Visible = true;

                    cargarDatosVisitante(idVisitante);
                }
            }
        }

        private void lbHabitante_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbHabitante.Text != "NA")
            {
                txtCantidad.Enabled = true;
                pTxtCantidad.BackColor = Color.LightGreen;
                txtCantidad.Focus();
                //txtPlacasVehiculo.Enabled = true;
                //pPlacas.BackColor = Color.LightGreen;
            }
            else 
            {
                txtPlacasVehiculo.Focus();
                txtCantidad.Enabled = false;
                pTxtCantidad.BackColor = Color.Red;
            }
        }

        private void lbVisitante_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (entra != 1)
            {
                string statusMasIdVisitante = statusVisitante();
                string[] splitStatusMasIdVisitante = statusMasIdVisitante.Split(new char[] { ',' });
                int status = Convert.ToInt32(splitStatusMasIdVisitante[0]);
                string idVisitante = splitStatusMasIdVisitante[1];

                if (status == 1)
                {
                    //cmdRegistrarIngreso.Visible = false;
                    //cmdRegistrarSalida.Visible = true;

                    cargarDatosVisitante(idVisitante);
                }
                else if (status == 0 && visitanteExiste() != 0)
                {
                    cargarImagen(idVisitante);
                    txtNombreVisitante.Enabled = false;
                    txtMotivo.Enabled = true;
                    pMotivo.BackColor = Color.LightGreen;
                    txtMotivo.Focus();
                }
            }
        }

        private void cbCamaraRostro_SelectedIndexChanged(object sender, EventArgs e)
        {
            camaraRostro.cargarVideo(cbCamaraRostro.SelectedIndex);
            cbCamaraRostro.Visible = false;
            lCamaraRostro.Visible = false;

        }

        private void cbCamaraCredencial_SelectedIndexChanged(object sender, EventArgs e)
        {
            camaraCredencial.cargarVideo(cbCamaraCredencial.SelectedIndex);
            lCamaraCredencial.Visible = false;
            cbCamaraCredencial.Visible = false;
        }
        //FIN EVENTOS INDEX
        
        //**
        
        //EVENTOS CAMBIO TEXTO
        private void txtPlacasVehiculo_TextChanged(object sender, EventArgs e)
        {
            if (txtPlacasVehiculo.TextLength > 3)
            {
                string placas = txtPlacasVehiculo.Text;
                string condicion = "idVisitante IN (SELECT idVisitante FROM Placas WHERE placa = '" + placas + "')";
                bd.llenarListBox(lbNombreVisitante, "nombre", "Visitante", condicion, "nombre", true, ref entra);

                if (lbNombreVisitante.Items.Count > 0)
                {
                    txtNombreVisitante.Visible = false;
                    txtNombreVisitante.Enabled = false;
                    pNombreVisitante.Visible = false;

                    pLbVisitante.Enabled = true;
                    pLbVisitante.BackColor = Color.LightGreen;
                    //lbNombreVisitante.Enabled = true;
                    lbNombreVisitante.Enabled = true;

                    cmdNuevoRegistro.Enabled = true;

                    txtMotivo.Enabled = false;
                    pMotivo.BackColor = Color.Red;
                }
                else
                {
                    pLbVisitante.Enabled = false;
                    lbNombreVisitante.Enabled = false;
                    pLbVisitante.BackColor = Color.Red;

                    txtNombreVisitante.Visible = true;
                    pNombreVisitante.Visible = true;
                    pNombreVisitante.BackColor = Color.Red;
                    cmdNuevoRegistro.Enabled = false;

                    pbCapturaRostro.Image = null;
                    pbCaturaCredencial.Image = null;

                    if (txtPlacasVehiculo.Text != "")
                    {
                        txtMotivo.Enabled = true;
                        pMotivo.BackColor = Color.LightGreen;
                    }
                    else
                    {
                        txtMotivo.Enabled = false;
                        pMotivo.BackColor = Color.Red;
                    }
                }
            }
            else
            {
                entra = 1;
                lbNombreVisitante.DataSource = null;
                lbNombreVisitante.Enabled = false;
                pLbVisitante.BackColor = Color.Red;
                cmdNuevoRegistro.Enabled = false;
                entra = 0;

                pbCapturaRostro.Image = null;
                pbCaturaCredencial.Image = null;

                if (txtPlacasVehiculo.Text != "" && txtPlacasVehiculo.TextLength > 3)
                {
                    txtMotivo.Enabled = true;
                    pMotivo.BackColor = Color.LightGreen;
                }
                else
                {
                    txtMotivo.Enabled = false;
                    pMotivo.BackColor = Color.Red;
                }
            }

            //if (txtPlacasVehiculo.Text != "")
            //{
            //    txtMotivo.Enabled = true;
            //    pMotivo.BackColor = Color.LightGreen;
            //}
            //else 
            //{
            //    txtMotivo.Enabled = false;
            //    pMotivo.BackColor = Color.Red;
            //}
        }

        private void txtNombreVisitante_TextChanged(object sender, EventArgs e)
        {
            if (txtNombreVisitante.TextLength > 5)
            {
                pbImagenRostro.Enabled = true;
                pImagenRostro.BackColor = Color.LightGreen;
            }
        }

        private void cbNombreVisitante_TextChanged(object sender, EventArgs e)
        {
            if (entra != 1)
            {
                txtMotivo.Enabled = true;
            }
        }

        private void txtMotivo_TextChanged(object sender, EventArgs e)
        {
            //cbNombre.Enabled = true;
            if (pLbVisitante.Enabled == true)
            {
                txtNombreVisitante.Enabled = false;
            }
            else
            {
                txtNombreVisitante.Enabled = true;
            }
            pNombreVisitante.BackColor = Color.LightGreen;
        }
        //FIN EVENTOS CAMBIO TEXTO
        
        //***
        
        //EVENTOS CLIC
        private void pbImagenRostro_Click(object sender, EventArgs e)
        {
            camaraRostro.capturarImagen();

            pbImagenRostro.Visible = false;
            pbCapturaRostro.Visible = true;
            pImagenRostro.BackColor = Color.Blue;
            pImagenCredencial.BackColor = Color.LightGreen;
            pbImagenCredencial.Enabled = true;
            //cmdCapturaRostro.Visible = false;
            //cmdNuevaFotoRostro.Visible = true;
            //cmdCapturarCredencial.Enabled = true;
        }

        private void pbCapturaRostro_Click(object sender, EventArgs e)
        {
            pbImagenRostro.Visible = true;
            pbCapturaRostro.Visible = false;
            pImagenRostro.BackColor = Color.LightGreen;
            //cmdCapturaRostro.Visible = true;
            //cmdNuevaFotoRostro.Visible = false;
        }

        private void pbImagenCredencial_Click(object sender, EventArgs e)
        {
            camaraCredencial.capturarImagen();

            pbImagenCredencial.Visible = false;
            pbCaturaCredencial.Visible = true;
            pImagenCredencial.BackColor = Color.Blue;

        }

        private void pbCaturaCredencial_Click(object sender, EventArgs e)
        {
            pbImagenCredencial.Visible = true;
            pbCaturaCredencial.Visible = false;
            pImagenCredencial.BackColor = Color.LightGreen;
        }

        private void cmdNuevoRegistro_Click(object sender, EventArgs e)
        {
            entra = 1;
            lbNombreVisitante.DataSource = null;
            entra = 0;
            lbNombreVisitante.Enabled = false;
            pLbVisitante.Enabled = false;
            pLbVisitante.BackColor = Color.Red;

            txtNombreVisitante.Visible = true;
            pNombreVisitante.Visible = true;
            pNombreVisitante.BackColor = Color.Red;
            cmdNuevoRegistro.Enabled = false;

            txtMotivo.Enabled = true;
            pMotivo.BackColor = Color.LightGreen;
            txtMotivo.Focus();

            pbCapturaRostro.Image = null;
            pbCaturaCredencial.Image = null;
            pbCapturaRostro.Visible = false;
            pbCaturaCredencial.Visible = false;
            pbImagenRostro.Visible = true;
            pbImagenCredencial.Visible = true;
        }
        //FIN EVENTOS CLIC
       
        //****
      
        //METODOS PROGRAMADOR
        public void desactivarBotones()
        {
            tsCmdAgregarHabitante.Enabled = false;
            tsCmdConsultaRegistro.Enabled = false;
            tsCmdAgregarGuardia.Enabled = false;
            tsLTipoUsuario.Text = "Guardia";
            lTurno.Visible = true;

            lNombre.Text = nombreUsuario.ToLower();
        }

        public void activarBotones()
        {
            tsCmdAgregarHabitante.Enabled = true;
            tsCmdConsultaRegistro.Enabled = true;
            tsCmdAgregarGuardia.Enabled = true;
            tsLTipoUsuario.Text = "Administrador";
            lTurno.Visible = false;
            lNombre.Text = nombreUsuario;
        }

        public string statusVisitante()
        {
            string status = "";
            string idVisitante = "";
            string sqlConsulta = "SELECT `idvisitante`,`status` FROM `Visitante` WHERE `nombre` = '" + lbNombreVisitante.Text + "'";

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(sqlConsulta);
            bd.adaptador.Fill(obtenerDatos, "Visitante");

            idVisitante = obtenerDatos.Tables[0].Rows[0]["idvisitante"].ToString();
            status = obtenerDatos.Tables[0].Rows[0]["status"].ToString();

            return status + "," + idVisitante;
        }

        public void cargarDatosVisitante(string idVisitante)
        {
            int index = 0;
            string horaEntrada = "";
            string fechaIngreso = "";
            //string placas = "";
            string motivo = "";
            //string idColono = "";
            //string nombreHabitante = "";
            string material = "";
            string cantidad = "";
            string empresa = "";
            string sqlConsulta = "SELECT `horaEntrada`,`fechaIngreso`,`motivo`,`material`,`cantidad`,`empresa`" +
                                 "FROM `Registro` AS `CARGARDATOS` WHERE `idIngreso` = (SELECT MAX(`idIngreso`) FROM `Registro` WHERE `idVisitante` = '" + idVisitante + "')";
                                 //"SELECT `horaEntrada`,`fechaIngreso`,`motivo`,`numeroCasa`,`idColono`" +
                                 //"FROM `Registro` AS `CARGARDATOS` WHERE `idIngreso` = (SELECT MAX(idIngreso) FROM `Registro` WHERE `idVisitante` = '" + idVisitante + "')";

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(sqlConsulta);
            bd.adaptador.Fill(obtenerDatos, "CARGARDATOS");

            horaEntrada = obtenerDatos.Tables["CARGARDATOS"].Rows[0]["horaEntrada"].ToString();
            fechaIngreso = obtenerDatos.Tables["CARGARDATOS"].Rows[0]["fechaIngreso"].ToString();
            //placas = obtenerDatos.Tables["CARGARDATOS"].Rows[0]["placas"].ToString();
            motivo = obtenerDatos.Tables["CARGARDATOS"].Rows[0]["motivo"].ToString();
            material = obtenerDatos.Tables["CARGARDATOS"].Rows[0]["material"].ToString();
            cantidad = obtenerDatos.Tables["CARGARDATOS"].Rows[0]["cantidad"].ToString();
            empresa = obtenerDatos.Tables["CARGARDATOS"].Rows[0]["empresa"].ToString();

            lHI.Visible = true;
            lFI.Visible = true;
            lFechaIngreso.Visible = true;
            lHoraIngreso.Visible = true;

            lHoraIngreso.Text = Convert.ToDateTime(horaEntrada).ToShortTimeString();
            lFechaIngreso.Text = Convert.ToDateTime(fechaIngreso).ToString("dd - MMMM - yy").ToUpper();
            //txtPlacasVehiculo.Text = placas;

            txtCantidad.Text = cantidad;
            cbCasaQueVisita.Text = empresa;
            txtMaterial.Visible = true;
            txtMaterial.Text = material;

            txtMotivo.Enabled = false;
            txtMotivo.Text = motivo;
            txtPlacasVehiculo.Enabled = false;
            //lbHabitante.Enabled = false;
            txtMaterial.Enabled = false;
            txtCantidad.Enabled = false;
            cbCasaQueVisita.Enabled = false;

            //entra = 1;
            //cbCasaQueVisita.DataSource = obtenerDatos.Tables["CARGARDATOS"];
            //entra = 0;
            //cbCasaQueVisita.DisplayMember = "numeroCasa";


            //nombreHabitante = habitanteVisitado(idColono);

            lbHabitante.Visible = false;
            pHabitante.Visible = false;

            //txtCargarHabitante.Visible = true;
            //pCargarHabitante.Visible = true;

            //txtCargarHabitante.Text = nombreHabitante;

            pMotivo.BackColor = Color.Red;
            lbNombreVisitante.Enabled = false;
            pLbVisitante.BackColor = Color.Red;

            cmdNuevoRegistro.Enabled = false;

            tsCmdRegistrarIngreso.Enabled = false;
            tsCmdRegistrarSalida.Enabled = true;
            //index = cbRepresentante.FindString(nombreHabitante);

            //entra = 1;
            //cbRepresentante.SelectedIndex = index;
            //entra = 0;

            cargarImagen(idVisitante);
        }

        public string habitanteVisitado(string idColono)
        {
            string nombreColono = "";
            string sqlConsulta = "SELECT `nombre` FROM `Colono` WHERE `idcolono` = '" + idColono + "'";

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(sqlConsulta);
            bd.adaptador.Fill(obtenerDatos, "Colono");

            nombreColono = obtenerDatos.Tables[0].Rows[0][0].ToString();

            return nombreColono;
        }

        public void cargarImagen(string idVisitante)
        {
            byte[] imagenRostro;
            byte[] imagenCredencial;

            string sqlConsulta = "SELECT `fotoRostro`,`fotoCredencial` FROM `Imagen` WHERE `idVisitante` = '" + idVisitante + "'";

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(sqlConsulta);
            bd.adaptador.Fill(obtenerDatos, "Imagen");

            imagenRostro = (byte[])obtenerDatos.Tables["Imagen"].Rows[0]["fotoRostro"];
            imagenCredencial = (byte[])obtenerDatos.Tables["Imagen"].Rows[0]["fotoCredencial"];

            MemoryStream msRostro = new MemoryStream(imagenRostro);
            MemoryStream msCredencial = new MemoryStream(imagenCredencial);

            //pbCapturaRostro.Visible = true;
            //pbCaturaCredencial.Visible = true;
            pbImagenRostro.Visible = false;
            pbImagenCredencial.Visible = false;

            pbCapturaRostro.Image = Image.FromStream(msRostro);
            pbCaturaCredencial.Image = Image.FromStream(msCredencial);
        }

        public int obtenerId()
        {
            int id = 0;
            string sqlConsulta = "SELECT MAX(idvisitante) FROM Visitante";

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(sqlConsulta);
            bd.adaptador.Fill(obtenerDatos, "Visitante");

            string recibeDato = obtenerDatos.Tables[0].Rows[0][0].ToString();

            id = Convert.ToInt32(recibeDato);
            return id;
        }

        public int visitanteExiste()
        {
            int id = 0;
            string sqlConsulta = "SELECT `idvisitante` FROM `Visitante`  WHERE `nombre` = '" + lbNombreVisitante.Text + "'";

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(sqlConsulta);
            bd.adaptador.Fill(obtenerDatos, "Visitante");

            try
            {
                string recibeDato = obtenerDatos.Tables[0].Rows[0][0].ToString();
                id = Convert.ToInt32(recibeDato);
                return id;
            }
            catch (IndexOutOfRangeException)
            {
                return id;
            }
        }

        public bool placaExiste(int idVisitante)
        {
            string placa = "";
            string sqlConsulta = "SELECT placa FROM Placas WHERE idVisitante = " + idVisitante + " and placa = '" + txtPlacasVehiculo.Text + "'";

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(sqlConsulta);
            bd.adaptador.Fill(obtenerDatos, "Placas");

            try
            {
                placa = obtenerDatos.Tables[0].Rows[0][0].ToString();

                if (placa != "")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (IndexOutOfRangeException)
            {

                return false;
            }
        }

        private void limpiar()
        {

            //cbNombre.Text = "";
            cbCasaQueVisita.Text = "";
            cbCasaQueVisita.Enabled = true;

            txtNombreVisitante.Text = "";
            txtPlacasVehiculo.Text = "";
            txtMotivo.Text = "";
            txtCantidad.Text = "";

            lbHabitante.DataSource = null;

            //pbCapturaRostro.Visible = false;
            pbImagenRostro.Visible = true;
            //pbCaturaCredencial.Visible = false;
            pbImagenCredencial.Visible = true;

            lHI.Visible = false;
            lFI.Visible = false;
            lFechaIngreso.Visible = false;
            lHoraIngreso.Visible = false;

            pHabitante.Visible = true;
            //pCargarHabitante.Visible = false;

            //txtCargarHabitante.Visible = false;

            txtPlacasVehiculo.Enabled = true;

            lbHabitante.Enabled = false;
            lbHabitante.Visible = true;

            txtMaterial.Visible = false;

            txtMotivo.Enabled = false;
            txtNombreVisitante.Enabled = false;

            pbCapturaRostro.Image = null;
            pbImagenCredencial.Image = null;

            //lTelefono.Text = "";

            cmdNuevoRegistro.Enabled = false;

            pHabitante.BackColor = Color.Red;
            pMotivo.BackColor = Color.Red;
            pNombreVisitante.BackColor = Color.Red;
            pLbVisitante.BackColor = Color.Red;
            pHabitante.BackColor = Color.Red;
            pImagenRostro.BackColor = Color.Red;
            pImagenCredencial.BackColor = Color.Red;
           

            tsCmdRegistrarIngreso.Enabled = true;
            tsCmdRegistrarSalida.Enabled = false;

            lbHabitante.SelectedIndex = -1;

            pTxtCantidad.BackColor = Color.Red;
            cbCasaQueVisita.Focus();

            bd.llenarComboBox(cbCasaQueVisita,sqlNumeroCasa, "nombre", "Empresa", ref entra);
        }
        //FIN METODOS 

        //*****

        //MENU TOOLSTRIP
        private void tsCmdAgregarHabitante_Click(object sender, EventArgs e)
        {
            AgregarHabitante habitante = new AgregarHabitante();

            habitante.Show();
        }

        private void tsCmdConsultaRegistro_Click(object sender, EventArgs e)
        {
            ConsultarRegistros consultarRegistros = new ConsultarRegistros();

            consultarRegistros.Show();
        }

        private void tsCmdAgregarGuardia_Click(object sender, EventArgs e)
        {
            AgregarGuardia agregarGuardia = new AgregarGuardia();

            agregarGuardia.Show();
        }

        private void tsCmdLimpiarDatos_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void tsCmdActualizar_Click(object sender, EventArgs e)
        {
            bd.llenarComboBox(cbCasaQueVisita, sqlNumeroCasa, "numero", "Casa", ref entra);

        }

        private void tsCmdConfiguracion_Click(object sender, EventArgs e)
        {
            cbCamaraCredencial.Visible = true;
            cbCamaraRostro.Visible = true;
            lCamaraCredencial.Visible = true;
            lCamaraRostro.Visible = true;
            tsCmdConfiguracion.Enabled = false;
        }

        private void tsCmdRegistrarIngreso_Click(object sender, EventArgs e)
        {
            if (lbHabitante.Text == "NA") 
            {
                txtCantidad.Text = "0";
            }

            if (/*lbNombreVisitante.Text != "" || txtNombreVisitante.Text != "" &&*/ txtPlacasVehiculo.Text != "" && txtMotivo.Text != "" && cbCasaQueVisita.SelectedIndex != -1 && lbHabitante.Text != ""  && txtCantidad.Text != "")
            {
                if (pbImagenRostro.Visible == false)
                {
                    int idVisitante = 0;
                    
                    string nombreVisitante = "";
                    string placas = txtPlacasVehiculo.Text;
                    string cantidad = txtCantidad.Text;
                    string material = lbHabitante.Text;
                    string fecha = DateTime.Today.ToShortDateString();
                    string hora = DateTime.Now.ToLongTimeString();
                    string motivo = txtMotivo.Text;
                    string sqlInsetarIngreso = "";
                    string sqlUpdateVisitante = "";
                    string sqliInsertarPlaca = "";
                    string empresa = cbCasaQueVisita.Text;
                    string idEmpresa = "(SELECT `idEmpresa` FROM `Empresa` WHERE `nombre` = '"+cbCasaQueVisita.Text+"' )";

                    //string idColono = "(SELECT `idcolono` FROM `Colono` WHERE `nombre` = '" + habitante + "' AND `numero` = " + numeroCasa + ")";

                    //string sqlInsertarVisitante = "INSERT INTO `Visitante`(`nombre`,`status`)" +
                    //                                             " VALUES ('" + nombreVisitante + "', 1);";
                    //id = visitanteExiste();


                    if (txtNombreVisitante.Enabled != true && txtNombreVisitante.Visible != true)
                    {
                        idVisitante = visitanteExiste();

                        sqliInsertarPlaca = "INSERT INTO `Placas`(`idVisitante`,`placa`) VALUES (" + idVisitante + ",'" + placas + "')";

                        sqlUpdateVisitante = "UPDATE `Visitante` SET `status`= 1 WHERE `idvisitante`= " + idVisitante + "";

                        sqlInsetarIngreso = "INSERT INTO `Registro`(`horaEntrada`,`horaSalida`,`fechaIngreso`,`idVisitante`,`motivo`,`material`,`cantidad`,`empresa`,`idGuardia`)" +
                                                           "VALUES ('" + hora + "', NULL, '" + fecha + "'," + idVisitante + ",  '" + motivo + "', '" + material + "', "+ cantidad +", '" + empresa + "'," + Convert.ToInt32(idGuardia) + ")";

                        if (bd.insertar(sqlInsetarIngreso) && bd.insertar(sqlUpdateVisitante))
                        {
                            MessageBox.Show("Registro ingresado correctamente");
                        }
                        else
                        {
                            MessageBox.Show("Error al intetar insertar datos");
                        }

                        if (!placaExiste(idVisitante))
                        {
                            bd.insertar(sqliInsertarPlaca);
                            //MessageBox.Show("placas insertadas correctamente para nuevo usuario");
                        }
                        //else
                        //{
                        //    bd.insertar(sqliInsertarPlaca);
                        //    MessageBox.Show("El registro de placas ya existe se registro a un nuevo visitante");
                        //}

                        //respuestaInsertarImagen = bd.guradarImagen(pbCapturaRostro, id);

                        //string sqlInsetar = "INSERT INTO `Registro`(`nombre`, `motivo`,`placas`,`horaentrada`,`horasalida`,`fechadeingreso`,`numero`,`idColono`,`status`) " +
                        //                      "VALUES ('" + nombreVisitante + "', NULL,'" + placas + "', '" + hora + "', NULL,'" + fecha + "'," + numeroCasa + "," + idColono + ", 1)";
                    }
                    else
                    {
                        nombreVisitante = txtNombreVisitante.Text;

                        string sqlInsertarVisitante = "INSERT INTO `Visitante`(`nombre`,`status`)" +
                                                                 " VALUES ('" + nombreVisitante + "', 1);";
                        bd.insertar(sqlInsertarVisitante);

                        idVisitante = obtenerId();

                        sqliInsertarPlaca = "INSERT INTO `Placas`(`idVisitante`,`placa`) VALUES (" + idVisitante + ",'" + placas + "')";

                        sqlInsetarIngreso = "INSERT INTO `Registro`(`horaEntrada`,`horaSalida`,`fechaIngreso`,`idVisitante`,`motivo`,`material`,`cantidad`,`empresa`,`idGuardia`) " +
                                                           "VALUES ('" + hora + "', NULL, '" + fecha + "'," + idVisitante + ",  '" + motivo + "', '" + material + "', " + cantidad + ", '" + empresa + "'," + Convert.ToInt32(idGuardia) + ")";

                        if (bd.insertar(sqlInsetarIngreso) && bd.guradarImagen(pbCapturaRostro, pbCaturaCredencial, idVisitante))
                        {
                            MessageBox.Show("Registro ingresado correctamente para nuevo usuario");
                        }
                        else
                        {
                            MessageBox.Show("Error al intetar insertar datos");
                        }

                        if (!placaExiste(idVisitante))
                        {
                            bd.insertar(sqliInsertarPlaca);
                            //MessageBox.Show("placas insertadas correctamente");
                        }
                        //else
                        //{
                        //    bd.insertar(sqliInsertarPlaca);
                        //    MessageBox.Show("El registro de placas ya existe se registro a un nuevo visitante");
                        //}
                    }
                    limpiar();
                    //bd.llenarComboBox(cbNombre, "nombre", "Visitante", "", "nombre", false, ref entra);
                    //cbCasaQueVisita.DataSource = null;

                    bd.llenarComboBox(cbCasaQueVisita, sqlNumeroCasa, "numero", "Casa", ref entra);

                }//if captura credencial 
                else
                {
                    MessageBox.Show("Favor de hacer la captura del rostro y la credencial");
                }
            }
            else
            {
                MessageBox.Show("Favor de introducir todos los datos");
            }
        }

        private void tsCmdRegistrarSalida_Click(object sender, EventArgs e)
        {
            bool respuestaActualizaRegistro = false;
            bool respuestaActualizaVisitante = false;
            string statusMasIdVisitante = statusVisitante();
            string[] splitStatusMasIdVisitante = statusMasIdVisitante.Split(new char[] { ',' });
            int status = Convert.ToInt32(splitStatusMasIdVisitante[0]);
            string idVisitante = splitStatusMasIdVisitante[1];
            string fechaSalida = DateTime.Today.ToShortDateString();
            string datos = "`horaSalida` = '" + lHora.Text + "', fechaSalida = '" + fechaSalida + "'";
            string datosStatus = "`status`= 0";

            string condicionActualizaRegistro = "`idIngreso`= (SELECT MAX(idIngreso) FROM `Registro` WHERE `idVisitante` = '" + idVisitante + "')";
            string condicionActualizaVisitante = "`idvisitante`= '" + idVisitante + "'";

            respuestaActualizaRegistro = bd.actualizar("`Registro`", datos, condicionActualizaRegistro);

            respuestaActualizaVisitante = bd.actualizar("`Visitante`", datosStatus, condicionActualizaVisitante);

            if (respuestaActualizaRegistro && respuestaActualizaVisitante)
            {
                MessageBox.Show("La salida se registro a las " + lHora.Text);
            }
            else
            {
                MessageBox.Show("Error al registrar la salida");
            }

            limpiar();

            //cbCasaQueVisita.DataSource = null;

            //bd.llenarComboBox(cbCasaQueVisita, sqlNumeroCasa, "numero", "Casa", ref entra);

            cbCasaQueVisita.Enabled = true;
            txtMaterial.Visible = false;
            lHoraIngreso.Visible = false;
            lFechaIngreso.Visible = false;
            lHI.Visible = false;
            lFI.Visible = false;
            tsCmdRegistrarIngreso.Enabled = true;
            tsCmdRegistrarSalida.Enabled = false;
            cbCasaQueVisita.Focus();
        }

        private void tsCmdCerrarSesion_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Restart();
        }

        //FIN MENU
    }
}

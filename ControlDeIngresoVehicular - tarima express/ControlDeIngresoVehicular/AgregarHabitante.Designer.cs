﻿namespace ControlDeIngresoVehicular
{
    partial class AgregarHabitante
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AgregarHabitante));
            this.label6 = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.gbDatosHabitante = new System.Windows.Forms.GroupBox();
            this.cmdAgregarHabitante = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.nuevoRegistroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gbDatosHabitante.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Nombre:";
            // 
            // txtNombre
            // 
            this.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombre.Location = new System.Drawing.Point(64, 54);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(251, 20);
            this.txtNombre.TabIndex = 5;
            // 
            // gbDatosHabitante
            // 
            this.gbDatosHabitante.Controls.Add(this.txtNombre);
            this.gbDatosHabitante.Controls.Add(this.label6);
            this.gbDatosHabitante.Controls.Add(this.cmdAgregarHabitante);
            this.gbDatosHabitante.Location = new System.Drawing.Point(12, 37);
            this.gbDatosHabitante.Name = "gbDatosHabitante";
            this.gbDatosHabitante.Size = new System.Drawing.Size(321, 144);
            this.gbDatosHabitante.TabIndex = 5;
            this.gbDatosHabitante.TabStop = false;
            this.gbDatosHabitante.Text = "Empresa";
            // 
            // cmdAgregarHabitante
            // 
            this.cmdAgregarHabitante.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAgregarHabitante.Location = new System.Drawing.Point(156, 102);
            this.cmdAgregarHabitante.Name = "cmdAgregarHabitante";
            this.cmdAgregarHabitante.Size = new System.Drawing.Size(159, 36);
            this.cmdAgregarHabitante.TabIndex = 6;
            this.cmdAgregarHabitante.Text = "Agregar empresa";
            this.cmdAgregarHabitante.UseVisualStyleBackColor = true;
            this.cmdAgregarHabitante.Click += new System.EventHandler(this.cmdAgregarHabitante_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevoRegistroToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(343, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // nuevoRegistroToolStripMenuItem
            // 
            this.nuevoRegistroToolStripMenuItem.Name = "nuevoRegistroToolStripMenuItem";
            this.nuevoRegistroToolStripMenuItem.Size = new System.Drawing.Size(97, 20);
            this.nuevoRegistroToolStripMenuItem.Text = "Nuevo registro";
            this.nuevoRegistroToolStripMenuItem.Click += new System.EventHandler(this.nuevoRegistroToolStripMenuItem_Click);
            // 
            // AgregarHabitante
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 192);
            this.Controls.Add(this.gbDatosHabitante);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "AgregarHabitante";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agregar empresa";
            this.gbDatosHabitante.ResumeLayout(false);
            this.gbDatosHabitante.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.GroupBox gbDatosHabitante;
        private System.Windows.Forms.Button cmdAgregarHabitante;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem nuevoRegistroToolStripMenuItem;
    }
}
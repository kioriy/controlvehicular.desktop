﻿namespace ControlDeIngresoVehicular
{
    partial class frmRegistroIngreso
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRegistroIngreso));
            this.pbImagenRostro = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPlacasVehiculo = new System.Windows.Forms.TextBox();
            this.cbCasaQueVisita = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lHora = new System.Windows.Forms.Label();
            this.lFecha = new System.Windows.Forms.Label();
            this.tReloj = new System.Windows.Forms.Timer(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.txtMotivo = new System.Windows.Forms.TextBox();
            this.lFechaIngreso = new System.Windows.Forms.Label();
            this.lHoraIngreso = new System.Windows.Forms.Label();
            this.lFI = new System.Windows.Forms.Label();
            this.lHI = new System.Windows.Forms.Label();
            this.pbCapturaRostro = new System.Windows.Forms.PictureBox();
            this.tsPrincipal = new System.Windows.Forms.ToolStrip();
            this.tsCmdAgregarHabitante = new System.Windows.Forms.ToolStripButton();
            this.tsCmdConsultaRegistro = new System.Windows.Forms.ToolStripButton();
            this.tsCmdAgregarGuardia = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsCmdLimpiarDatos = new System.Windows.Forms.ToolStripButton();
            this.tsCmdActualizar = new System.Windows.Forms.ToolStripButton();
            this.tsCmdConfiguracion = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.tsCmdRegistrarIngreso = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel5 = new System.Windows.Forms.ToolStripLabel();
            this.tsCmdRegistrarSalida = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel6 = new System.Windows.Forms.ToolStripLabel();
            this.tsLTipoUsuario = new System.Windows.Forms.ToolStripLabel();
            this.tsCmdCerrarSesion = new System.Windows.Forms.ToolStripButton();
            this.lbPlacas = new System.Windows.Forms.ListBox();
            this.lbHabitante = new System.Windows.Forms.ListBox();
            this.pPlacas = new System.Windows.Forms.Panel();
            this.pMotivo = new System.Windows.Forms.Panel();
            this.lNombre = new System.Windows.Forms.Label();
            this.cmdNuevoRegistro = new System.Windows.Forms.Button();
            this.pImagenRostro = new System.Windows.Forms.Panel();
            this.pImagenCredencial = new System.Windows.Forms.Panel();
            this.cbCamaraRostro = new System.Windows.Forms.ComboBox();
            this.cbCamaraCredencial = new System.Windows.Forms.ComboBox();
            this.lCamaraRostro = new System.Windows.Forms.Label();
            this.lCamaraCredencial = new System.Windows.Forms.Label();
            this.lTurno = new System.Windows.Forms.Label();
            this.pCasaQueVisita = new System.Windows.Forms.Panel();
            this.pHabitante = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCantidad = new System.Windows.Forms.TextBox();
            this.pTxtCantidad = new System.Windows.Forms.Panel();
            this.pbCaturaCredencial = new System.Windows.Forms.PictureBox();
            this.pbImagenCredencial = new System.Windows.Forms.PictureBox();
            this.pLbVisitante = new System.Windows.Forms.Panel();
            this.pNombreVisitante = new System.Windows.Forms.Panel();
            this.txtNombreVisitante = new System.Windows.Forms.TextBox();
            this.lbNombreVisitante = new System.Windows.Forms.ListBox();
            this.txtMaterial = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagenRostro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCapturaRostro)).BeginInit();
            this.tsPrincipal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCaturaCredencial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagenCredencial)).BeginInit();
            this.pNombreVisitante.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbImagenRostro
            // 
            this.pbImagenRostro.Enabled = false;
            this.pbImagenRostro.Location = new System.Drawing.Point(12, 90);
            this.pbImagenRostro.Name = "pbImagenRostro";
            this.pbImagenRostro.Size = new System.Drawing.Size(639, 551);
            this.pbImagenRostro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbImagenRostro.TabIndex = 0;
            this.pbImagenRostro.TabStop = false;
            this.pbImagenRostro.Click += new System.EventHandler(this.pbImagenRostro_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(657, 274);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nombre visitante:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(661, 219);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Placas Vehiculo:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(701, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Empresa:";
            // 
            // txtPlacasVehiculo
            // 
            this.txtPlacasVehiculo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPlacasVehiculo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlacasVehiculo.Location = new System.Drawing.Point(775, 213);
            this.txtPlacasVehiculo.Name = "txtPlacasVehiculo";
            this.txtPlacasVehiculo.Size = new System.Drawing.Size(114, 29);
            this.txtPlacasVehiculo.TabIndex = 4;
            this.txtPlacasVehiculo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPlacasVehiculo.TextChanged += new System.EventHandler(this.txtPlacasVehiculo_TextChanged);
            // 
            // cbCasaQueVisita
            // 
            this.cbCasaQueVisita.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbCasaQueVisita.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbCasaQueVisita.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCasaQueVisita.FormattingEnabled = true;
            this.cbCasaQueVisita.Location = new System.Drawing.Point(775, 91);
            this.cbCasaQueVisita.Name = "cbCasaQueVisita";
            this.cbCasaQueVisita.Size = new System.Drawing.Size(178, 24);
            this.cbCasaQueVisita.TabIndex = 1;
            this.cbCasaQueVisita.SelectedIndexChanged += new System.EventHandler(this.cbCasaQueVisita_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(693, 150);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 16);
            this.label6.TabIndex = 13;
            this.label6.Text = "Mercancia:";
            // 
            // lHora
            // 
            this.lHora.AutoSize = true;
            this.lHora.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lHora.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lHora.ForeColor = System.Drawing.Color.Lime;
            this.lHora.Location = new System.Drawing.Point(516, 601);
            this.lHora.Name = "lHora";
            this.lHora.Size = new System.Drawing.Size(98, 20);
            this.lHora.TabIndex = 25;
            this.lHora.Text = "A qui estoy";
            // 
            // lFecha
            // 
            this.lFecha.AutoSize = true;
            this.lFecha.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lFecha.ForeColor = System.Drawing.Color.Lime;
            this.lFecha.Location = new System.Drawing.Point(22, 601);
            this.lFecha.Name = "lFecha";
            this.lFecha.Size = new System.Drawing.Size(98, 20);
            this.lFecha.TabIndex = 27;
            this.lFecha.Text = "A qui estoy";
            // 
            // tReloj
            // 
            this.tReloj.Interval = 1000;
            this.tReloj.Tick += new System.EventHandler(this.tReloj_Tick);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(902, 219);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 16);
            this.label5.TabIndex = 39;
            this.label5.Text = "Motivo:";
            // 
            // txtMotivo
            // 
            this.txtMotivo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMotivo.Enabled = false;
            this.txtMotivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMotivo.Location = new System.Drawing.Point(959, 207);
            this.txtMotivo.Multiline = true;
            this.txtMotivo.Name = "txtMotivo";
            this.txtMotivo.Size = new System.Drawing.Size(288, 41);
            this.txtMotivo.TabIndex = 5;
            this.txtMotivo.TextChanged += new System.EventHandler(this.txtMotivo_TextChanged);
            // 
            // lFechaIngreso
            // 
            this.lFechaIngreso.AutoSize = true;
            this.lFechaIngreso.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lFechaIngreso.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lFechaIngreso.ForeColor = System.Drawing.Color.Lime;
            this.lFechaIngreso.Location = new System.Drawing.Point(775, 314);
            this.lFechaIngreso.Name = "lFechaIngreso";
            this.lFechaIngreso.Size = new System.Drawing.Size(91, 20);
            this.lFechaIngreso.TabIndex = 45;
            this.lFechaIngreso.Text = "aqui estoy";
            this.lFechaIngreso.Visible = false;
            // 
            // lHoraIngreso
            // 
            this.lHoraIngreso.AutoSize = true;
            this.lHoraIngreso.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lHoraIngreso.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lHoraIngreso.ForeColor = System.Drawing.Color.Lime;
            this.lHoraIngreso.Location = new System.Drawing.Point(775, 351);
            this.lHoraIngreso.Name = "lHoraIngreso";
            this.lHoraIngreso.Size = new System.Drawing.Size(91, 20);
            this.lHoraIngreso.TabIndex = 44;
            this.lHoraIngreso.Text = "aqui estoy";
            this.lHoraIngreso.Visible = false;
            // 
            // lFI
            // 
            this.lFI.AutoSize = true;
            this.lFI.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lFI.Location = new System.Drawing.Point(672, 316);
            this.lFI.Name = "lFI";
            this.lFI.Size = new System.Drawing.Size(97, 16);
            this.lFI.TabIndex = 43;
            this.lFI.Text = "Fecha ingreso:";
            this.lFI.Visible = false;
            // 
            // lHI
            // 
            this.lHI.AutoSize = true;
            this.lHI.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lHI.Location = new System.Drawing.Point(680, 353);
            this.lHI.Name = "lHI";
            this.lHI.Size = new System.Drawing.Size(89, 16);
            this.lHI.TabIndex = 42;
            this.lHI.Text = "Hora ingreso:";
            this.lHI.Visible = false;
            // 
            // pbCapturaRostro
            // 
            this.pbCapturaRostro.ErrorImage = null;
            this.pbCapturaRostro.InitialImage = null;
            this.pbCapturaRostro.Location = new System.Drawing.Point(12, 91);
            this.pbCapturaRostro.Name = "pbCapturaRostro";
            this.pbCapturaRostro.Size = new System.Drawing.Size(639, 550);
            this.pbCapturaRostro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbCapturaRostro.TabIndex = 28;
            this.pbCapturaRostro.TabStop = false;
            this.pbCapturaRostro.Click += new System.EventHandler(this.pbCapturaRostro_Click);
            // 
            // tsPrincipal
            // 
            this.tsPrincipal.BackColor = System.Drawing.SystemColors.Desktop;
            this.tsPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsCmdAgregarHabitante,
            this.tsCmdConsultaRegistro,
            this.tsCmdAgregarGuardia,
            this.toolStripSeparator1,
            this.tsCmdLimpiarDatos,
            this.tsCmdActualizar,
            this.tsCmdConfiguracion,
            this.toolStripLabel4,
            this.tsCmdRegistrarIngreso,
            this.toolStripButton4,
            this.toolStripLabel5,
            this.tsCmdRegistrarSalida,
            this.toolStripLabel6,
            this.tsLTipoUsuario,
            this.tsCmdCerrarSesion});
            this.tsPrincipal.Location = new System.Drawing.Point(0, 0);
            this.tsPrincipal.Name = "tsPrincipal";
            this.tsPrincipal.Size = new System.Drawing.Size(1254, 77);
            this.tsPrincipal.TabIndex = 47;
            this.tsPrincipal.Text = "toolStrip1";
            // 
            // tsCmdAgregarHabitante
            // 
            this.tsCmdAgregarHabitante.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.tsCmdAgregarHabitante.Image = ((System.Drawing.Image)(resources.GetObject("tsCmdAgregarHabitante.Image")));
            this.tsCmdAgregarHabitante.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsCmdAgregarHabitante.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsCmdAgregarHabitante.Name = "tsCmdAgregarHabitante";
            this.tsCmdAgregarHabitante.Size = new System.Drawing.Size(101, 74);
            this.tsCmdAgregarHabitante.Text = "Agregar empresa";
            this.tsCmdAgregarHabitante.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.tsCmdAgregarHabitante.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsCmdAgregarHabitante.Click += new System.EventHandler(this.tsCmdAgregarHabitante_Click);
            // 
            // tsCmdConsultaRegistro
            // 
            this.tsCmdConsultaRegistro.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.tsCmdConsultaRegistro.Image = ((System.Drawing.Image)(resources.GetObject("tsCmdConsultaRegistro.Image")));
            this.tsCmdConsultaRegistro.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsCmdConsultaRegistro.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsCmdConsultaRegistro.Name = "tsCmdConsultaRegistro";
            this.tsCmdConsultaRegistro.Size = new System.Drawing.Size(110, 74);
            this.tsCmdConsultaRegistro.Text = "Consultar registros";
            this.tsCmdConsultaRegistro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsCmdConsultaRegistro.Click += new System.EventHandler(this.tsCmdConsultaRegistro_Click);
            // 
            // tsCmdAgregarGuardia
            // 
            this.tsCmdAgregarGuardia.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.tsCmdAgregarGuardia.Image = ((System.Drawing.Image)(resources.GetObject("tsCmdAgregarGuardia.Image")));
            this.tsCmdAgregarGuardia.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsCmdAgregarGuardia.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsCmdAgregarGuardia.Name = "tsCmdAgregarGuardia";
            this.tsCmdAgregarGuardia.Size = new System.Drawing.Size(96, 74);
            this.tsCmdAgregarGuardia.Text = "Agregar guardia";
            this.tsCmdAgregarGuardia.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsCmdAgregarGuardia.Click += new System.EventHandler(this.tsCmdAgregarGuardia_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 77);
            // 
            // tsCmdLimpiarDatos
            // 
            this.tsCmdLimpiarDatos.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.tsCmdLimpiarDatos.Image = ((System.Drawing.Image)(resources.GetObject("tsCmdLimpiarDatos.Image")));
            this.tsCmdLimpiarDatos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsCmdLimpiarDatos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsCmdLimpiarDatos.Name = "tsCmdLimpiarDatos";
            this.tsCmdLimpiarDatos.Size = new System.Drawing.Size(84, 74);
            this.tsCmdLimpiarDatos.Text = "Limpiar datos";
            this.tsCmdLimpiarDatos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsCmdLimpiarDatos.Click += new System.EventHandler(this.tsCmdLimpiarDatos_Click);
            // 
            // tsCmdActualizar
            // 
            this.tsCmdActualizar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.tsCmdActualizar.Image = ((System.Drawing.Image)(resources.GetObject("tsCmdActualizar.Image")));
            this.tsCmdActualizar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsCmdActualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsCmdActualizar.Name = "tsCmdActualizar";
            this.tsCmdActualizar.Size = new System.Drawing.Size(84, 74);
            this.tsCmdActualizar.Text = "Actualizar";
            this.tsCmdActualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsCmdActualizar.Click += new System.EventHandler(this.tsCmdActualizar_Click);
            // 
            // tsCmdConfiguracion
            // 
            this.tsCmdConfiguracion.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tsCmdConfiguracion.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.tsCmdConfiguracion.Image = ((System.Drawing.Image)(resources.GetObject("tsCmdConfiguracion.Image")));
            this.tsCmdConfiguracion.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsCmdConfiguracion.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsCmdConfiguracion.Name = "tsCmdConfiguracion";
            this.tsCmdConfiguracion.Size = new System.Drawing.Size(87, 74);
            this.tsCmdConfiguracion.Text = "Configuracion";
            this.tsCmdConfiguracion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsCmdConfiguracion.Click += new System.EventHandler(this.tsCmdConfiguracion_Click);
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(86, 74);
            this.toolStripLabel4.Text = "toolStripLabel4";
            // 
            // tsCmdRegistrarIngreso
            // 
            this.tsCmdRegistrarIngreso.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsCmdRegistrarIngreso.ForeColor = System.Drawing.Color.Lime;
            this.tsCmdRegistrarIngreso.Image = ((System.Drawing.Image)(resources.GetObject("tsCmdRegistrarIngreso.Image")));
            this.tsCmdRegistrarIngreso.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsCmdRegistrarIngreso.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsCmdRegistrarIngreso.Name = "tsCmdRegistrarIngreso";
            this.tsCmdRegistrarIngreso.Size = new System.Drawing.Size(107, 74);
            this.tsCmdRegistrarIngreso.Text = "Registrar Entrada";
            this.tsCmdRegistrarIngreso.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsCmdRegistrarIngreso.Click += new System.EventHandler(this.tsCmdRegistrarIngreso_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripButton4.ForeColor = System.Drawing.Color.Red;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(0, 74);
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripLabel5
            // 
            this.toolStripLabel5.Name = "toolStripLabel5";
            this.toolStripLabel5.Size = new System.Drawing.Size(86, 74);
            this.toolStripLabel5.Text = "toolStripLabel5";
            // 
            // tsCmdRegistrarSalida
            // 
            this.tsCmdRegistrarSalida.Enabled = false;
            this.tsCmdRegistrarSalida.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsCmdRegistrarSalida.ForeColor = System.Drawing.Color.Red;
            this.tsCmdRegistrarSalida.Image = ((System.Drawing.Image)(resources.GetObject("tsCmdRegistrarSalida.Image")));
            this.tsCmdRegistrarSalida.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsCmdRegistrarSalida.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsCmdRegistrarSalida.Name = "tsCmdRegistrarSalida";
            this.tsCmdRegistrarSalida.Size = new System.Drawing.Size(95, 74);
            this.tsCmdRegistrarSalida.Text = "Registrar salida";
            this.tsCmdRegistrarSalida.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsCmdRegistrarSalida.Click += new System.EventHandler(this.tsCmdRegistrarSalida_Click);
            // 
            // toolStripLabel6
            // 
            this.toolStripLabel6.Name = "toolStripLabel6";
            this.toolStripLabel6.Size = new System.Drawing.Size(86, 74);
            this.toolStripLabel6.Text = "toolStripLabel6";
            // 
            // tsLTipoUsuario
            // 
            this.tsLTipoUsuario.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsLTipoUsuario.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.tsLTipoUsuario.Name = "tsLTipoUsuario";
            this.tsLTipoUsuario.Size = new System.Drawing.Size(91, 74);
            this.tsLTipoUsuario.Text = "Administrador";
            // 
            // tsCmdCerrarSesion
            // 
            this.tsCmdCerrarSesion.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsCmdCerrarSesion.ForeColor = System.Drawing.Color.Yellow;
            this.tsCmdCerrarSesion.Image = ((System.Drawing.Image)(resources.GetObject("tsCmdCerrarSesion.Image")));
            this.tsCmdCerrarSesion.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsCmdCerrarSesion.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsCmdCerrarSesion.Name = "tsCmdCerrarSesion";
            this.tsCmdCerrarSesion.Size = new System.Drawing.Size(83, 74);
            this.tsCmdCerrarSesion.Text = "Cerrar sesion";
            this.tsCmdCerrarSesion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsCmdCerrarSesion.Click += new System.EventHandler(this.tsCmdCerrarSesion_Click);
            // 
            // lbPlacas
            // 
            this.lbPlacas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPlacas.FormattingEnabled = true;
            this.lbPlacas.ItemHeight = 16;
            this.lbPlacas.Location = new System.Drawing.Point(775, 201);
            this.lbPlacas.Name = "lbPlacas";
            this.lbPlacas.Size = new System.Drawing.Size(114, 52);
            this.lbPlacas.TabIndex = 48;
            this.lbPlacas.Visible = false;
            // 
            // lbHabitante
            // 
            this.lbHabitante.Enabled = false;
            this.lbHabitante.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHabitante.FormattingEnabled = true;
            this.lbHabitante.ItemHeight = 16;
            this.lbHabitante.Items.AddRange(new object[] {
            "TARIMA",
            "TABLA",
            "TIRA",
            "POLIN",
            "NA"});
            this.lbHabitante.Location = new System.Drawing.Point(775, 125);
            this.lbHabitante.Name = "lbHabitante";
            this.lbHabitante.Size = new System.Drawing.Size(180, 68);
            this.lbHabitante.TabIndex = 2;
            this.lbHabitante.SelectedIndexChanged += new System.EventHandler(this.lbHabitante_SelectedIndexChanged);
            // 
            // pPlacas
            // 
            this.pPlacas.BackColor = System.Drawing.Color.Yellow;
            this.pPlacas.Location = new System.Drawing.Point(773, 211);
            this.pPlacas.Name = "pPlacas";
            this.pPlacas.Size = new System.Drawing.Size(118, 33);
            this.pPlacas.TabIndex = 52;
            // 
            // pMotivo
            // 
            this.pMotivo.BackColor = System.Drawing.Color.Red;
            this.pMotivo.Location = new System.Drawing.Point(957, 205);
            this.pMotivo.Name = "pMotivo";
            this.pMotivo.Size = new System.Drawing.Size(292, 45);
            this.pMotivo.TabIndex = 53;
            // 
            // lNombre
            // 
            this.lNombre.AutoSize = true;
            this.lNombre.BackColor = System.Drawing.Color.Black;
            this.lNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lNombre.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lNombre.Location = new System.Drawing.Point(1042, 53);
            this.lNombre.Name = "lNombre";
            this.lNombre.Size = new System.Drawing.Size(57, 16);
            this.lNombre.TabIndex = 55;
            this.lNombre.Text = "Nombre";
            // 
            // cmdNuevoRegistro
            // 
            this.cmdNuevoRegistro.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.cmdNuevoRegistro.Enabled = false;
            this.cmdNuevoRegistro.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdNuevoRegistro.ForeColor = System.Drawing.Color.Lime;
            this.cmdNuevoRegistro.Location = new System.Drawing.Point(1138, 252);
            this.cmdNuevoRegistro.Name = "cmdNuevoRegistro";
            this.cmdNuevoRegistro.Size = new System.Drawing.Size(111, 61);
            this.cmdNuevoRegistro.TabIndex = 58;
            this.cmdNuevoRegistro.Text = "Registrar Nuevo Visistante mismas placas";
            this.cmdNuevoRegistro.UseVisualStyleBackColor = false;
            this.cmdNuevoRegistro.Click += new System.EventHandler(this.cmdNuevoRegistro_Click);
            // 
            // pImagenRostro
            // 
            this.pImagenRostro.BackColor = System.Drawing.Color.Red;
            this.pImagenRostro.Location = new System.Drawing.Point(7, 86);
            this.pImagenRostro.Name = "pImagenRostro";
            this.pImagenRostro.Size = new System.Drawing.Size(649, 560);
            this.pImagenRostro.TabIndex = 52;
            // 
            // pImagenCredencial
            // 
            this.pImagenCredencial.BackColor = System.Drawing.Color.Red;
            this.pImagenCredencial.Location = new System.Drawing.Point(690, 378);
            this.pImagenCredencial.Name = "pImagenCredencial";
            this.pImagenCredencial.Size = new System.Drawing.Size(542, 268);
            this.pImagenCredencial.TabIndex = 53;
            // 
            // cbCamaraRostro
            // 
            this.cbCamaraRostro.FormattingEnabled = true;
            this.cbCamaraRostro.Location = new System.Drawing.Point(868, 333);
            this.cbCamaraRostro.Name = "cbCamaraRostro";
            this.cbCamaraRostro.Size = new System.Drawing.Size(121, 21);
            this.cbCamaraRostro.TabIndex = 59;
            this.cbCamaraRostro.Visible = false;
            this.cbCamaraRostro.SelectedIndexChanged += new System.EventHandler(this.cbCamaraRostro_SelectedIndexChanged);
            // 
            // cbCamaraCredencial
            // 
            this.cbCamaraCredencial.FormattingEnabled = true;
            this.cbCamaraCredencial.Location = new System.Drawing.Point(1126, 333);
            this.cbCamaraCredencial.Name = "cbCamaraCredencial";
            this.cbCamaraCredencial.Size = new System.Drawing.Size(121, 21);
            this.cbCamaraCredencial.TabIndex = 60;
            this.cbCamaraCredencial.Visible = false;
            this.cbCamaraCredencial.SelectedIndexChanged += new System.EventHandler(this.cbCamaraCredencial_SelectedIndexChanged);
            // 
            // lCamaraRostro
            // 
            this.lCamaraRostro.AutoSize = true;
            this.lCamaraRostro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lCamaraRostro.Location = new System.Drawing.Point(760, 335);
            this.lCamaraRostro.Name = "lCamaraRostro";
            this.lCamaraRostro.Size = new System.Drawing.Size(96, 16);
            this.lCamaraRostro.TabIndex = 61;
            this.lCamaraRostro.Text = "Camara rostro:";
            this.lCamaraRostro.Visible = false;
            // 
            // lCamaraCredencial
            // 
            this.lCamaraCredencial.AutoSize = true;
            this.lCamaraCredencial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lCamaraCredencial.Location = new System.Drawing.Point(995, 335);
            this.lCamaraCredencial.Name = "lCamaraCredencial";
            this.lCamaraCredencial.Size = new System.Drawing.Size(125, 16);
            this.lCamaraCredencial.TabIndex = 62;
            this.lCamaraCredencial.Text = "Camara credencial:";
            this.lCamaraCredencial.Visible = false;
            // 
            // lTurno
            // 
            this.lTurno.AutoSize = true;
            this.lTurno.BackColor = System.Drawing.Color.Black;
            this.lTurno.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lTurno.ForeColor = System.Drawing.Color.Transparent;
            this.lTurno.Location = new System.Drawing.Point(1040, 8);
            this.lTurno.Name = "lTurno";
            this.lTurno.Size = new System.Drawing.Size(43, 16);
            this.lTurno.TabIndex = 54;
            this.lTurno.Text = "Turno";
            // 
            // pCasaQueVisita
            // 
            this.pCasaQueVisita.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.pCasaQueVisita.Location = new System.Drawing.Point(773, 90);
            this.pCasaQueVisita.Name = "pCasaQueVisita";
            this.pCasaQueVisita.Size = new System.Drawing.Size(182, 26);
            this.pCasaQueVisita.TabIndex = 50;
            // 
            // pHabitante
            // 
            this.pHabitante.BackColor = System.Drawing.Color.Red;
            this.pHabitante.Location = new System.Drawing.Point(773, 123);
            this.pHabitante.Name = "pHabitante";
            this.pHabitante.Size = new System.Drawing.Size(184, 72);
            this.pHabitante.TabIndex = 51;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(974, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 16);
            this.label4.TabIndex = 63;
            this.label4.Text = "Cantidad:";
            // 
            // txtCantidad
            // 
            this.txtCantidad.Enabled = false;
            this.txtCantidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCantidad.Location = new System.Drawing.Point(1045, 141);
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.Size = new System.Drawing.Size(108, 35);
            this.txtCantidad.TabIndex = 3;
            this.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pTxtCantidad
            // 
            this.pTxtCantidad.BackColor = System.Drawing.Color.Red;
            this.pTxtCantidad.Location = new System.Drawing.Point(1043, 139);
            this.pTxtCantidad.Name = "pTxtCantidad";
            this.pTxtCantidad.Size = new System.Drawing.Size(112, 39);
            this.pTxtCantidad.TabIndex = 51;
            // 
            // pbCaturaCredencial
            // 
            this.pbCaturaCredencial.Location = new System.Drawing.Point(695, 383);
            this.pbCaturaCredencial.Name = "pbCaturaCredencial";
            this.pbCaturaCredencial.Size = new System.Drawing.Size(532, 258);
            this.pbCaturaCredencial.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbCaturaCredencial.TabIndex = 33;
            this.pbCaturaCredencial.TabStop = false;
            this.pbCaturaCredencial.Click += new System.EventHandler(this.pbCaturaCredencial_Click);
            // 
            // pbImagenCredencial
            // 
            this.pbImagenCredencial.Enabled = false;
            this.pbImagenCredencial.Location = new System.Drawing.Point(696, 383);
            this.pbImagenCredencial.Name = "pbImagenCredencial";
            this.pbImagenCredencial.Size = new System.Drawing.Size(531, 258);
            this.pbImagenCredencial.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbImagenCredencial.TabIndex = 8;
            this.pbImagenCredencial.TabStop = false;
            this.pbImagenCredencial.Click += new System.EventHandler(this.pbImagenCredencial_Click);
            // 
            // pLbVisitante
            // 
            this.pLbVisitante.BackColor = System.Drawing.Color.Red;
            this.pLbVisitante.Enabled = false;
            this.pLbVisitante.Location = new System.Drawing.Point(773, 262);
            this.pLbVisitante.Name = "pLbVisitante";
            this.pLbVisitante.Size = new System.Drawing.Size(361, 40);
            this.pLbVisitante.TabIndex = 52;
            // 
            // pNombreVisitante
            // 
            this.pNombreVisitante.BackColor = System.Drawing.Color.Red;
            this.pNombreVisitante.Controls.Add(this.txtNombreVisitante);
            this.pNombreVisitante.Location = new System.Drawing.Point(773, 268);
            this.pNombreVisitante.Name = "pNombreVisitante";
            this.pNombreVisitante.Size = new System.Drawing.Size(476, 26);
            this.pNombreVisitante.TabIndex = 51;
            // 
            // txtNombreVisitante
            // 
            this.txtNombreVisitante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombreVisitante.Enabled = false;
            this.txtNombreVisitante.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreVisitante.Location = new System.Drawing.Point(2, 2);
            this.txtNombreVisitante.Name = "txtNombreVisitante";
            this.txtNombreVisitante.Size = new System.Drawing.Size(472, 22);
            this.txtNombreVisitante.TabIndex = 6;
            this.txtNombreVisitante.TextChanged += new System.EventHandler(this.txtNombreVisitante_TextChanged);
            // 
            // lbNombreVisitante
            // 
            this.lbNombreVisitante.Enabled = false;
            this.lbNombreVisitante.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNombreVisitante.FormattingEnabled = true;
            this.lbNombreVisitante.ItemHeight = 16;
            this.lbNombreVisitante.Location = new System.Drawing.Point(775, 264);
            this.lbNombreVisitante.Name = "lbNombreVisitante";
            this.lbNombreVisitante.Size = new System.Drawing.Size(357, 36);
            this.lbNombreVisitante.TabIndex = 6;
            this.lbNombreVisitante.SelectedIndexChanged += new System.EventHandler(this.lbVisitante_SelectedIndexChanged);
            // 
            // txtMaterial
            // 
            this.txtMaterial.Location = new System.Drawing.Point(773, 148);
            this.txtMaterial.Name = "txtMaterial";
            this.txtMaterial.Size = new System.Drawing.Size(184, 20);
            this.txtMaterial.TabIndex = 64;
            // 
            // frmRegistroIngreso
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1254, 656);
            this.Controls.Add(this.txtCantidad);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lCamaraCredencial);
            this.Controls.Add(this.lCamaraRostro);
            this.Controls.Add(this.cbCamaraRostro);
            this.Controls.Add(this.cbCamaraCredencial);
            this.Controls.Add(this.cmdNuevoRegistro);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pNombreVisitante);
            this.Controls.Add(this.lNombre);
            this.Controls.Add(this.lTurno);
            this.Controls.Add(this.lbHabitante);
            this.Controls.Add(this.lFecha);
            this.Controls.Add(this.lHora);
            this.Controls.Add(this.lFechaIngreso);
            this.Controls.Add(this.lHoraIngreso);
            this.Controls.Add(this.lFI);
            this.Controls.Add(this.lHI);
            this.Controls.Add(this.pbImagenRostro);
            this.Controls.Add(this.pbCapturaRostro);
            this.Controls.Add(this.tsPrincipal);
            this.Controls.Add(this.txtMotivo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbCasaQueVisita);
            this.Controls.Add(this.pbImagenCredencial);
            this.Controls.Add(this.txtPlacasVehiculo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pbCaturaCredencial);
            this.Controls.Add(this.pCasaQueVisita);
            this.Controls.Add(this.pTxtCantidad);
            this.Controls.Add(this.pHabitante);
            this.Controls.Add(this.pPlacas);
            this.Controls.Add(this.pMotivo);
            this.Controls.Add(this.lbPlacas);
            this.Controls.Add(this.pImagenRostro);
            this.Controls.Add(this.pImagenCredencial);
            this.Controls.Add(this.lbNombreVisitante);
            this.Controls.Add(this.pLbVisitante);
            this.Controls.Add(this.txtMaterial);
            this.MaximizeBox = false;
            this.Name = "frmRegistroIngreso";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Control de ingreso vehicular";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmRegistroIngreso_FormClosed);
            this.Load += new System.EventHandler(this.frmRegistroIngreso_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbImagenRostro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCapturaRostro)).EndInit();
            this.tsPrincipal.ResumeLayout(false);
            this.tsPrincipal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCaturaCredencial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagenCredencial)).EndInit();
            this.pNombreVisitante.ResumeLayout(false);
            this.pNombreVisitante.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPlacasVehiculo;
        private System.Windows.Forms.ComboBox cbCasaQueVisita;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lHora;
        private System.Windows.Forms.Label lFecha;
        public System.Windows.Forms.PictureBox pbImagenRostro;
        private System.Windows.Forms.Timer tReloj;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtMotivo;
        private System.Windows.Forms.Label lFechaIngreso;
        private System.Windows.Forms.Label lHoraIngreso;
        private System.Windows.Forms.Label lFI;
        private System.Windows.Forms.Label lHI;
        public System.Windows.Forms.PictureBox pbCapturaRostro;
        private System.Windows.Forms.ToolStrip tsPrincipal;
        private System.Windows.Forms.ToolStripButton tsCmdAgregarHabitante;
        private System.Windows.Forms.ToolStripButton tsCmdConsultaRegistro;
        private System.Windows.Forms.ToolStripButton tsCmdAgregarGuardia;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.ToolStripButton tsCmdRegistrarIngreso;
        private System.Windows.Forms.ToolStripLabel toolStripButton4;
        private System.Windows.Forms.ToolStripLabel toolStripLabel5;
        private System.Windows.Forms.ToolStripButton tsCmdRegistrarSalida;
        private System.Windows.Forms.ToolStripLabel toolStripLabel6;
        private System.Windows.Forms.ToolStripLabel tsLTipoUsuario;
        private System.Windows.Forms.ToolStripButton tsCmdCerrarSesion;
        private System.Windows.Forms.ToolStripButton tsCmdLimpiarDatos;
        private System.Windows.Forms.ListBox lbPlacas;
        private System.Windows.Forms.ToolStripButton tsCmdActualizar;
        private System.Windows.Forms.ListBox lbHabitante;
        private System.Windows.Forms.Panel pPlacas;
        private System.Windows.Forms.Panel pMotivo;
        private System.Windows.Forms.Label lNombre;
        private System.Windows.Forms.Button cmdNuevoRegistro;
        private System.Windows.Forms.Panel pImagenRostro;
        private System.Windows.Forms.Panel pImagenCredencial;
        private System.Windows.Forms.ToolStripButton tsCmdConfiguracion;
        private System.Windows.Forms.ComboBox cbCamaraRostro;
        private System.Windows.Forms.ComboBox cbCamaraCredencial;
        private System.Windows.Forms.Label lCamaraRostro;
        private System.Windows.Forms.Label lCamaraCredencial;
        private System.Windows.Forms.Label lTurno;
        private System.Windows.Forms.Panel pCasaQueVisita;
        private System.Windows.Forms.Panel pHabitante;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCantidad;
        private System.Windows.Forms.Panel pTxtCantidad;
        private System.Windows.Forms.PictureBox pbCaturaCredencial;
        private System.Windows.Forms.PictureBox pbImagenCredencial;
        private System.Windows.Forms.Panel pLbVisitante;
        private System.Windows.Forms.Panel pNombreVisitante;
        private System.Windows.Forms.ListBox lbNombreVisitante;
        private System.Windows.Forms.TextBox txtNombreVisitante;
        private System.Windows.Forms.TextBox txtMaterial;
    }
}


﻿namespace ControlDeIngresoVehicular
{
    partial class AgregarGuardia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdAgregarGuardia = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbMostrarContraseña = new System.Windows.Forms.CheckBox();
            this.cbNombreGuardia = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtVerificaContraseña = new System.Windows.Forms.TextBox();
            this.cbPermisos = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtContraseña = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tsNuevoRegistro = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdModificarGuardia = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmdAgregarGuardia
            // 
            this.cmdAgregarGuardia.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAgregarGuardia.Location = new System.Drawing.Point(182, 226);
            this.cmdAgregarGuardia.Name = "cmdAgregarGuardia";
            this.cmdAgregarGuardia.Size = new System.Drawing.Size(140, 36);
            this.cmdAgregarGuardia.TabIndex = 9;
            this.cmdAgregarGuardia.Text = "Agregar guardia";
            this.cmdAgregarGuardia.UseVisualStyleBackColor = true;
            this.cmdAgregarGuardia.Click += new System.EventHandler(this.cmdAgregarGuardia_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbMostrarContraseña);
            this.groupBox1.Controls.Add(this.cbNombreGuardia);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtVerificaContraseña);
            this.groupBox1.Controls.Add(this.cbPermisos);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtContraseña);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtNombre);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 40);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(320, 180);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos guardia";
            // 
            // cbMostrarContraseña
            // 
            this.cbMostrarContraseña.AutoSize = true;
            this.cbMostrarContraseña.Location = new System.Drawing.Point(160, 77);
            this.cbMostrarContraseña.Name = "cbMostrarContraseña";
            this.cbMostrarContraseña.Size = new System.Drawing.Size(117, 17);
            this.cbMostrarContraseña.TabIndex = 3;
            this.cbMostrarContraseña.Text = "Mostrar contraseña";
            this.cbMostrarContraseña.UseVisualStyleBackColor = true;
            this.cbMostrarContraseña.CheckedChanged += new System.EventHandler(this.cbMostrarContraseña_CheckedChanged);
            // 
            // cbNombreGuardia
            // 
            this.cbNombreGuardia.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbNombreGuardia.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbNombreGuardia.FormattingEnabled = true;
            this.cbNombreGuardia.Location = new System.Drawing.Point(160, 47);
            this.cbNombreGuardia.Name = "cbNombreGuardia";
            this.cbNombreGuardia.Size = new System.Drawing.Size(150, 21);
            this.cbNombreGuardia.TabIndex = 2;
            this.cbNombreGuardia.SelectedIndexChanged += new System.EventHandler(this.cbNombreGuardia_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(50, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Verificar contraseña:";
            // 
            // txtVerificaContraseña
            // 
            this.txtVerificaContraseña.Location = new System.Drawing.Point(160, 126);
            this.txtVerificaContraseña.Name = "txtVerificaContraseña";
            this.txtVerificaContraseña.Size = new System.Drawing.Size(150, 20);
            this.txtVerificaContraseña.TabIndex = 5;
            this.txtVerificaContraseña.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtVerificaContraseña.UseSystemPasswordChar = true;
            // 
            // cbPermisos
            // 
            this.cbPermisos.FormattingEnabled = true;
            this.cbPermisos.ItemHeight = 13;
            this.cbPermisos.Items.AddRange(new object[] {
            "GUARDIA",
            "ADMINISTRADOR"});
            this.cbPermisos.Location = new System.Drawing.Point(160, 152);
            this.cbPermisos.Name = "cbPermisos";
            this.cbPermisos.Size = new System.Drawing.Size(150, 21);
            this.cbPermisos.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(102, 156);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Permisos:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(90, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Contraseña:";
            // 
            // txtContraseña
            // 
            this.txtContraseña.Location = new System.Drawing.Point(160, 100);
            this.txtContraseña.Name = "txtContraseña";
            this.txtContraseña.Size = new System.Drawing.Size(150, 20);
            this.txtContraseña.TabIndex = 4;
            this.txtContraseña.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtContraseña.UseSystemPasswordChar = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(55, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Nombre de usuario:";
            // 
            // txtNombre
            // 
            this.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombre.Location = new System.Drawing.Point(60, 20);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(251, 20);
            this.txtNombre.TabIndex = 1;
            this.txtNombre.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nombre:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsNuevoRegistro});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(344, 24);
            this.menuStrip1.TabIndex = 12;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tsNuevoRegistro
            // 
            this.tsNuevoRegistro.Name = "tsNuevoRegistro";
            this.tsNuevoRegistro.Size = new System.Drawing.Size(97, 20);
            this.tsNuevoRegistro.Text = "Nuevo registro";
            this.tsNuevoRegistro.Click += new System.EventHandler(this.tsNuevoRegistro_Click);
            // 
            // cmdModificarGuardia
            // 
            this.cmdModificarGuardia.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdModificarGuardia.ForeColor = System.Drawing.Color.MediumBlue;
            this.cmdModificarGuardia.Location = new System.Drawing.Point(129, 226);
            this.cmdModificarGuardia.Name = "cmdModificarGuardia";
            this.cmdModificarGuardia.Size = new System.Drawing.Size(193, 36);
            this.cmdModificarGuardia.TabIndex = 7;
            this.cmdModificarGuardia.Text = "Modificar datos guardia";
            this.cmdModificarGuardia.UseVisualStyleBackColor = true;
            this.cmdModificarGuardia.Visible = false;
            this.cmdModificarGuardia.Click += new System.EventHandler(this.cmdModificarGuardia_Click);
            // 
            // AgregarGuardia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 268);
            this.Controls.Add(this.cmdModificarGuardia);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.cmdAgregarGuardia);
            this.Name = "AgregarGuardia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agregar guardia";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmdAgregarGuardia;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbPermisos;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtContraseña;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsNuevoRegistro;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtVerificaContraseña;
        private System.Windows.Forms.ComboBox cbNombreGuardia;
        private System.Windows.Forms.Button cmdModificarGuardia;
        private System.Windows.Forms.CheckBox cbMostrarContraseña;
    }
}
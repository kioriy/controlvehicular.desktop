﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ControlDeIngresoVehicular
{
    public partial class ConsultarRegistros : Form
    {
        BD bd = new BD();
        DataSet obtenerDatos;
        int entra = 0;

        public ConsultarRegistros()
        {
            InitializeComponent();
            string select = "v.nombre, r.numeroCasa, r.horaEntrada, r.horaSalida, r.fechaIngreso, r.fechaSalida, u.nombreUsuario ";
            string from = "Visitante AS v, Registro AS r, Usuario AS u";
            string condicion = "r.idVisitante = v.idvisitante AND u.idGuardia = r.idGuardia";
            //string select = "v.nombre as `Visitante`, r.numeroCasa, c.nombre as `Habitante visitado`, r.horaEntrada, r.horaSalida, r.fechaIngreso";
            //string from = "Visitante AS v, Registro AS r, Colono AS c";
            //string condicion = "v.idVisitante = r.idVisitante and c.idcolono = r.idColono";
            string queryCbGuardia = "SELECT `nombreUsuario` FROM `Usuario` ORDER BY `nombreUsuario` ASC";
            string queryCbVisitante = "SELECT DISTINCT `nombre` FROM `Visitante` ORDER BY `nombre` ASC";

            bd.llenarComboBox(cbNombreGuardia, queryCbGuardia, "nombreUsuario", "Usuario", ref entra);
            bd.llenarComboBox(cbVisitante, queryCbVisitante, "nombre", "`Visitante`", ref entra);
            bd.llenarDgv(dgvConsultaRegistro, select, from, condicion, "v.nombre");
        }

        private void cmdConsulta_Click(object sender, EventArgs e)
        {
            string select = "v.nombre, r.numeroCasa, r.horaEntrada, r.horaSalida, r.fechaIngreso, r.fechaSalida, u.nombreUsuario ";
            string from = "Visitante AS v, Registro AS r, Usuario AS u";
            string condicion = "r.idVisitante = v.idvisitante AND u.idGuardia = r.idGuardia";

            string fecha = dtpFecha.Value.ToShortDateString();
            string guardia = cbNombreGuardia.Text;
            string visitante = cbVisitante.Text;
            string condicionFecha = " AND r.fechaIngreso = '" + fecha + "'";
            string condicionGuardia = " AND u.nombreUsuario = '" + guardia + "'";
            string condicionVisitante = " AND v.nombre = '" + visitante + "'";

            if (chkConFecha.Checked)
            {
                condicion += condicionFecha;
            }

            if (cbNombreGuardia.SelectedIndex > -1 && guardia != "") 
            {
                condicion += condicionGuardia;
            }

            if (cbVisitante.SelectedIndex > -1 && visitante != "") 
            {
                condicion += condicionVisitante;
            }
            
            bd.llenarDgv(dgvConsultaRegistro, select, from, condicion, "v.nombre");

            cbNombreGuardia.SelectedIndex = -1;
            cbVisitante.SelectedIndex = -1;
        }

        private void cmdMostrarTodo_Click(object sender, EventArgs e)
        {
            string select = "v.nombre, r.numeroCasa, r.horaEntrada, r.horaSalida, r.fechaIngreso, r.fechaSalida, u.nombreUsuario ";
            string from = "Visitante AS v, Registro AS r, Usuario AS u";
            string condicion = "r.idVisitante = v.idvisitante AND u.idGuardia = r.idGuardia";

            bd.llenarDgv(dgvConsultaRegistro, select, from, condicion, "v.nombre");

            cbNombreGuardia.SelectedIndex = -1;
            cbVisitante.SelectedIndex = -1;
        }

        private void dgvConsultaRegistro_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //pbCapturaRostro = null;
            //pbCaturaCredencial = null;
            try
            {
                string nombreVisitante = dgvConsultaRegistro[0, e.RowIndex].Value.ToString();

                int id = idVisitante(nombreVisitante);

               bd.cargarImagen(id, pbImagenRostro,pbImagenCredencial);
            }
            catch (Exception) { }
        }
        
        //public void cargarImagen(int idVisitante)
        //{
        //    string sqlConsulta = "SELECT `fotoRostro`,`fotoCredencial` FROM `Imagen` WHERE `idVisitante` = '" + idVisitante + "'";


        //    obtenerDatos = new DataSet();
        //    obtenerDatos.Reset();

        //    bd.adaptador = bd.consultaAdaptador(sqlConsulta);
        //    bd.adaptador.Fill(obtenerDatos, "Imagen");

        //    string rutaImagenRostro = obtenerDatos.Tables["Imagen"].Rows[0]["fotoRostro"].ToString();
        //    string rutaImagenCredencial = obtenerDatos.Tables["Imagen"].Rows[0]["fotoCredencial"].ToString();

        //    // MessageBox.Show(rutaImagenCredencial+rutaImagenRostro);

        //    pbImagenRostro.Image = Image.FromFile(rutaImagenRostro);
        //    pbImagenCredencial.Image = Image.FromFile(rutaImagenCredencial);

        //    //MemoryStream msRostro = new MemoryStream(imagenRostro);
        //    //MemoryStream msCredencial = new MemoryStream(imagenCredencial);

        //    //pbCapturaRostro.Visible = true;
        //    //pbCaturaCredencial.Visible = true;
        //    //pbImagenRostro.Visible = false;
        //    //pbImagenCredencial.Visible = false;

        //    /*
        //    byte[] imagenRostro;
        //    byte[] imagenCredencial;

        //    string sqlConsulta = "SELECT `fotoRostro`,`fotoCredencial` FROM `Imagen` WHERE `idVisitante` = " + idVisitante + "";

        //    obtenerDatos = new DataSet();
        //    obtenerDatos.Reset();

        //    bd.adaptador = bd.consultaAdaptador(sqlConsulta);
        //    bd.adaptador.Fill(obtenerDatos, "Imagen");

        //    imagenRostro = (byte[])obtenerDatos.Tables["Imagen"].Rows[0]["fotoRostro"];
        //    imagenCredencial = (byte[])obtenerDatos.Tables["Imagen"].Rows[0]["fotoCredencial"];

        //    MemoryStream msRostro = new MemoryStream(imagenRostro);
        //    MemoryStream msCredencial = new MemoryStream(imagenCredencial);

        //    //pbCapturaRostro.Visible = true;
        //    //pbCaturaCredencial.Visible = true;
        //    //pbImagenRostro.Visible = false;
        //    //pbImagenCredencial.Visible = false;

        //    pbImagenRostro.Image = Image.FromStream(msRostro);
        //    pbImagenCredencial.Image = Image.FromStream(msCredencial);*/
        //}

        public int idVisitante(string nombreVisitante)
        {
            int id = 0;
            string sqlConsulta = "SELECT `idvisitante` FROM `Visitante`  WHERE `nombre` = '" + nombreVisitante + "'";

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(sqlConsulta);
            bd.adaptador.Fill(obtenerDatos, "Visitante");

            try
            {
                string recibeDato = obtenerDatos.Tables[0].Rows[0][0].ToString();
                id = Convert.ToInt32(recibeDato);
                return id;
            }
            catch (IndexOutOfRangeException)
            {
                return id;
            }
        }

        private void guardarImagen_Click(object sender, EventArgs e)
        {
             
            //int columna= dgvConsultaRegistro.CurrentCell.RowIndex;
            int fila = dgvConsultaRegistro.CurrentCell.RowIndex;

            guardarImagen(pbImagenRostro,dgvConsultaRegistro[0,fila].Value.ToString()+" Rostro");
            guardarImagen(pbImagenCredencial, dgvConsultaRegistro[0, fila].Value.ToString() + " Credencial");

        }
        public void guardarImagen(PictureBox imagenGuardar, string nombreImagen)
        {
            try
            {
            
                SaveFileDialog Guardar = new SaveFileDialog();
                Guardar.FileName = nombreImagen;
                Guardar.Filter = "JPEG(*.JPG)|*.JPG|BMP(*.BMP)|*.BMP";
                Image Imagen = imagenGuardar.Image;

                Guardar.ShowDialog();

                Imagen.Save(Guardar.FileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Algo salio mal, Vuelve a intentarlo");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlDeIngresoVehicular
{
    public partial class AgregarGuardia : Form
    {
        BD bd = new BD();
        DataSet obtenerDatos;
        int entra = 0;
        int idGuardia = 0;
        string sqliUsuario = "SELECT `nombreUsuario` FROM `Usuario` ORDER BY `nombreUsuario` ASC";

        public AgregarGuardia()
        {
            InitializeComponent();
            
            bd.llenarComboBox(cbNombreGuardia,sqliUsuario, "nombreUsuario", "Usuario", ref entra);
        }

        //EVENTOS CLIC
        private void cmdAgregarGuardia_Click(object sender, EventArgs e)
        {
            if (txtContraseña.Text == txtVerificaContraseña.Text)
            {
                if (txtNombre.Text != "" && cbNombreGuardia.Text != "" && txtContraseña.Text != "")
                {
                    string nombre = txtNombre.Text;

                    string nombreUsuario = cbNombreGuardia.Text.ToUpper().Trim();
                    string contraseña = txtContraseña.Text.ToLower().Trim();
                    int permisos = -1;

                    if (cbPermisos.Text == "GUARDIA")
                    {
                        permisos = 1;
                    }
                    else if (cbPermisos.Text == "ADMINISTRADOR")
                    {
                        permisos = 0;
                    }

                    //string sqlInsertarGuardia = "INSERT INTO `Guardia`(`nombre`) VALUES ('" + nombre + "')";

                    //bool respuesta = bd.insertar(sqlInsertarGuardia);

                    //idGuardia = this.idGuardia();

                    string sqlInsertarUsuario = "INSERT INTO `Usuario`(`tipo`,`nombreUsuario`,`contraseña`,`nombre`) "+
                                                "VALUES ('" + permisos + "','" + nombreUsuario + "','" + contraseña + "', '" + nombre + "')";

                    if (bd.insertar(sqlInsertarUsuario))
                    {
                        MessageBox.Show("Datos de guardia insertados correctamente");
                        cmdAgregarGuardia.Enabled = false;
                    }
                    else
                    {
                        MessageBox.Show("Error al intetar insertar datos");
                    }
                }
                else
                {
                    MessageBox.Show("Favor de llenar todos los datos del guardia");
                }
            }
            else
            {
                MessageBox.Show("Las Contraseñas no coiciden favor de verificar");
            }
        }

        private void cmdModificarGuardia_Click(object sender, EventArgs e)
        {
            if (txtContraseña.Text == txtVerificaContraseña.Text)
            {
                string nombre = txtNombre.Text;

                string nombreUsuario = cbNombreGuardia.Text.ToUpper().Trim();
                string contraseña = txtContraseña.Text.ToLower().Trim();
                int permisos = -1;

                if (cbPermisos.Text == "GUARDIA")
                {
                    permisos = 1;
                }
                else if (cbPermisos.Text == "ADMINISTRADOR")
                {
                    permisos = 0;
                }

                string tabla = "`Usuario`";
                string datos = "`tipo`= " + permisos + ", `nombreUsuario`= '" + nombreUsuario + "', `contraseña`= '" + contraseña + "', `nombre` = '" + nombre + "'";
                string condicion = "`idGuardia` = '" + idGuardia + "'";

                //string sqlModificarUsuario = "UPDATE `Usuario` SET `tipo`= '" + permisos + "', `nombreUsuario`= '" + nombreUsuario 
                //+ "', `contraseña`= '" + contraseña + "', `nombre` = '" + nombre + "' WHERE `nombreUsuario` = '" + nombreUsuario + "'";

                if (bd.actualizar(tabla, datos, condicion))
                {
                    MessageBox.Show("Datos de guardia modificados");
                }
                else
                {
                    MessageBox.Show("Error al intetar insertar datos");
                }
                cmdModificarGuardia.Enabled = false;

                bd.llenarComboBox(cbNombreGuardia, sqliUsuario, "nombreUsuario", "Usuario", ref entra);
            }
            else
            {
                MessageBox.Show("Las Contraseñas no coiciden favor de verificar");
            }
        }

        private void tsNuevoRegistro_Click(object sender, EventArgs e)
        {
            cmdAgregarGuardia.Enabled = true;
            limpiar();
        }       
        //FIN EVENTOS CLIC
     
        private void limpiar() 
        {
            txtNombre.Text = "";
            txtContraseña.Text = "";
            txtVerificaContraseña.Text = "";
            cbNombreGuardia.Text = "";

            cmdModificarGuardia.Visible = false;

            entra = 1;
            cbPermisos.SelectedIndex = -1;
            entra = 0;
        }

        private void cbNombreGuardia_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (entra != 1)
            {
                cmdModificarGuardia.Visible = true;
                cmdModificarGuardia.Enabled = true;
                
                string nombre = "";
                string nombreUsuario = cbNombreGuardia.Text;
                string contraseña;
                string permisos = "";
                string sqlConsulta = "SELECT `nombre`,`tipo`, `contraseña`, `idGuardia`  FROM `Usuario` WHERE `nombreUsuario` = '"+nombreUsuario+"'";
                string idGuardia = "";

                obtenerDatos = new DataSet();
                obtenerDatos.Reset();

                bd.adaptador = bd.consultaAdaptador(sqlConsulta);
                bd.adaptador.Fill(obtenerDatos, "Usuario");

                nombre = obtenerDatos.Tables["Usuario"].Rows[0]["nombre"].ToString();
                permisos = obtenerDatos.Tables["Usuario"].Rows[0]["tipo"].ToString();
                contraseña = obtenerDatos.Tables["Usuario"].Rows[0]["contraseña"].ToString();
                idGuardia = obtenerDatos.Tables["Usuario"].Rows[0]["idGuardia"].ToString();

                this.idGuardia = Convert.ToInt32(idGuardia);

                txtNombre.Text = nombre;
                txtContraseña.Text = contraseña;
              
                if (permisos == "0") 
                {
                    cbPermisos.SelectedIndex = 1;
                }
                else 
                {
                    cbPermisos.SelectedIndex = 0;
                }

                cmdModificarGuardia.Visible = true;
            }
        }

        private void cbMostrarContraseña_CheckedChanged(object sender, EventArgs e)
        {
            if (cbMostrarContraseña.Checked)
            {
                txtContraseña.UseSystemPasswordChar = false;
                txtVerificaContraseña.UseSystemPasswordChar = false;
            }
            else 
            {
                txtContraseña.UseSystemPasswordChar = true;
                txtVerificaContraseña.UseSystemPasswordChar = true;
            }
        }

        //public int idGuardia() 
        //{
        //    int id = 0;
        //    string sqlConsulta = "SELECT MAX(idGuardia) FROM Guardia";

        //    obtenerDatos = new DataSet();
        //    obtenerDatos.Reset();

        //    bd.adaptador = bd.consultaAdaptador(sqlConsulta);
        //    bd.adaptador.Fill(obtenerDatos, "Guardia");

        //    string recibeDato = obtenerDatos.Tables[0].Rows[0][0].ToString();

        //    id = Convert.ToInt32(recibeDato);
        //    return id;
        //}
    }
}

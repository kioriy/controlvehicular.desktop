﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using System.IO;
using System.Windows.Media.Imaging;

namespace ControlDeIngresoVehicular
{        
    public partial class frmRegistroIngreso : Form
    {

        
        Video camaraRostro;
        Video camaraCredencial;
        Escaner e = new Escaner();
        BD bd = new BD();
        DataSet obtenerDatos;
        string idGuardia = "";
        string permisos = "";
        string nombreUsuario = ""; 
        public int entra = 0;
        string sqlNumeroCasa = "SELECT `numero` FROM `Casa` ORDER BY `numero` ASC";
        int idCamaraRostro=0;
        int idCamaraCredencial=5;
        bool banderaCamara;
        //EVENTOS CARGA FORMULARIO

        public frmRegistroIngreso(string idGuardia, string permisos, string nombreUsuario)
        {
            InitializeComponent();

            this.idGuardia = idGuardia;
            this.permisos = permisos;
            this.nombreUsuario = nombreUsuario;

            if (permisos == "0") 
            {
                activarBotones();
            }
            else if (permisos == "1") 
            {
                desactivarBotones();
            }

            camaraRostro = new Video(pbVideoRostro, pbCapturaRostro);
            camaraCredencial = new Video(pbVideoCredencial, pbCaturaCredencial);
           
           
        }

        private void frmRegistroIngreso_Load(object sender, EventArgs e)
        {
            //camaraRostro.cargarVideo(0);
            //camaraCredencial.cargarVideo(1);
            camaraRostro.cargarComboBox(cbCamaraRostro);
            camaraCredencial.cargarComboBox(cbCamaraCredencial);
            
            camaraRostro.cargarVideo(0);
            camaraCredencial.cargarVideo(1);
            
            tReloj.Start();
            alertaIntencion.Start();
            lFecha.Text = DateTime.Today.ToString("dd - MMMM - yyyy").ToUpper();
            bd.llenarComboBox(cbCasaQueVisita, sqlNumeroCasa, "numero", "Casa", ref entra);
            
            //bd.llenarComboBox(cbNombre, "nombre", "Visitante", "", "nombre", false, ref entra);
        }

        private void frmRegistroIngreso_FormClosed(object sender, FormClosedEventArgs e)
        {
            camaraRostro.detenerVideo();
            camaraCredencial.detenerVideo();
            tReloj.Stop();
            Application.Exit();
        }

        private void tReloj_Tick(object sender, EventArgs e)
        {
            lHora.Text = DateTime.Now.ToLongTimeString();
        }
        //FIN EVENTOS CARGA FORMULARIO
        
        //*
        
        //EVENTOS CAMBIO INDEX
        private void cbCasaQueVisita_SelectedIndexChanged(object sender, EventArgs e)
        {
            string numero = cbCasaQueVisita.Text;

            if (/*numero != "System.Data.DataRowView" && numero != ""*/entra != 1)
            {
                //cbRepresentante.DataSource = null;
                bd.llenarListBox(lbHabitante, "nombre", "Colono", "numero = "+numero, "nombre", true,ref entra);

                //cargar Telefono
                obtenerDatos = new DataSet();
                obtenerDatos.Reset();
                
                bd.adaptador = bd.consultaAdaptador("SELECT telefono FROM Casa WHERE numero = '" + cbCasaQueVisita.Text + "'");
                bd.adaptador.Fill(obtenerDatos, "Casa");

                string recibeDato = obtenerDatos.Tables[0].Rows[0][0].ToString();

                lTelefono.Text = Convert.ToString(recibeDato);

                //cbRepresentante.Enabled = true;
                lbHabitante.Enabled = true;
                pHabitante.BackColor = Color.LightGreen;
            }
        }

        private void lbHabitante_SelectedIndexChanged(object sender, EventArgs e)
        {
            //txtPlacasVehiculo.Enabled = true;
            //pPlacas.BackColor = Color.LightGreen;
        }

        private void lbVisitante_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (entra != 1)
            {
                string statusMasIdVisitante = statusVisitante();
                string[] splitStatusMasIdVisitante = statusMasIdVisitante.Split(',');
                int status = Convert.ToInt32(splitStatusMasIdVisitante[0]);
                string idVisitante = splitStatusMasIdVisitante[1];

                if (status == 1)
                {

                    //cmdRegistrarIngreso.Visible = false;
                    //cmdRegistrarSalida.Visible = true;

                    cargarDatosVisitante(idVisitante);

                    pbCapturaRostro.Visible = true;
                    pbCaturaCredencial.Visible = true;
                    pbVideoRostro.Visible = false;
                    pbVideoCredencial.Visible = false;

                }
                else if (status == 0 && visitanteExiste() != 0)
                {

                    bd.cargarImagen(int.Parse(idVisitante), pbCapturaRostro, pbCaturaCredencial);
                    txtNombreVisitante.Enabled = false;
                    txtMotivo.Enabled = true;
                    pMotivo.BackColor = Color.LightGreen;
                    txtMotivo.Focus();
                    pbCapturaRostro.Visible = true;
                    pbCaturaCredencial.Visible = true;
                    pbVideoRostro.Visible = false;
                    pbVideoCredencial.Visible = false;

                }
            }
        }

        private void cbCamaraRostro_SelectedIndexChanged(object sender, EventArgs e)
        {
            idCamaraRostro = cbCamaraRostro.SelectedIndex;
            camaraRostro.cargarVideo(idCamaraRostro);
            cbCamaraRostro.Visible = false;
            lCamaraRostro.Visible = false;

        }

        private void cbCamaraCredencial_SelectedIndexChanged(object sender, EventArgs e)
        {
            idCamaraCredencial = cbCamaraCredencial.SelectedIndex;
            camaraCredencial.cargarVideo(idCamaraCredencial);
            lCamaraCredencial.Visible = false;
            cbCamaraCredencial.Visible = false;
        }
        //FIN EVENTOS INDEX
        
        //**
        
        //EVENTOS CAMBIO TEXTO
        private void txtPlacasVehiculo_TextChanged(object sender, EventArgs e)
        {
            if (txtPlacasVehiculo.TextLength > 3)
            {
                string placas = txtPlacasVehiculo.Text;
                string condicion = "idVisitante IN (SELECT idVisitante FROM Placas WHERE placa = '" + placas + "')";
                bd.llenarListBox(lbNombreVisitante, "nombre", "Visitante", condicion, "nombre", true, ref entra);

                if (lbNombreVisitante.Items.Count > 0)
                {
                    txtNombreVisitante.Visible = false;
                    txtNombreVisitante.Enabled = false;
                    pNombreVisitante.Visible = false;

                    pLbVisitante.Visible = true;
                    pLbVisitante.BackColor = Color.LightGreen;
                    lbNombreVisitante.Visible = true;
                    lbNombreVisitante.Enabled = true;

                    cmdNuevoRegistro.Visible = true;

                    txtMotivo.Enabled = false;
                    pMotivo.BackColor = Color.Red;
                }
                else
                {
                    pLbVisitante.Visible = false;
                    lbNombreVisitante.Visible = false;

                    txtNombreVisitante.Visible = true;
                    pNombreVisitante.Visible = true;
                    pNombreVisitante.BackColor = Color.Red;
                    cmdNuevoRegistro.Visible = false;

                    pbCapturaRostro.Image = null;
                    pbCaturaCredencial.Image = null;

                    if (txtPlacasVehiculo.Text != "")
                    {
                        txtMotivo.Enabled = true;
                        pMotivo.BackColor = Color.LightGreen;
                    }
                    else
                    {
                        txtMotivo.Enabled = false;
                        pMotivo.BackColor = Color.Red;
                    }
                }
            }
            else
            {
                entra = 1;
                lbNombreVisitante.DataSource = null;
                lbNombreVisitante.Enabled = false;
                pLbVisitante.BackColor = Color.Red;
                cmdNuevoRegistro.Visible = false;
                entra = 0;

                pbCapturaRostro.Image = null;
                pbCaturaCredencial.Image = null;

                if (txtPlacasVehiculo.Text != "" && txtPlacasVehiculo.TextLength > 3)
                {
                    txtMotivo.Enabled = true;
                    pMotivo.BackColor = Color.LightGreen;
                }
                else
                {
                    txtMotivo.Enabled = false;
                    pMotivo.BackColor = Color.Red;
                }
            }

            //if (txtPlacasVehiculo.Text != "")
            //{
            //    txtMotivo.Enabled = true;
            //    pMotivo.BackColor = Color.LightGreen;
            //}
            //else 
            //{
            //    txtMotivo.Enabled = false;
            //    pMotivo.BackColor = Color.Red;
            //}
        }

        private void txtNombreVisitante_TextChanged(object sender, EventArgs e)
        {
            if (txtNombreVisitante.TextLength > 5)
            {
                pbVideoRostro.Enabled = true;
                pImagenRostro.BackColor = Color.LightGreen;
            }
        }

        private void cbNombreVisitante_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtMotivo_TextChanged(object sender, EventArgs e)
        {
            //cbNombre.Enabled = true;
            if (pLbVisitante.Visible == true)
            {
                txtNombreVisitante.Enabled = false;
            }
            else
            {
                txtNombreVisitante.Enabled = true;
            }
            pNombreVisitante.BackColor = Color.LightGreen;
        }
        //FIN EVENTOS CAMBIO TEXTO
        
        //***
        
        //EVENTOS CLIC
        

        private void pbCapturaRostro_Click(object sender, EventArgs e)
        {
            pbVideoRostro.Visible = true;
            pbCapturaRostro.Visible = false;
            pImagenRostro.BackColor = Color.LightGreen;
            //cmdCapturaRostro.Visible = true;
            //cmdNuevaFotoRostro.Visible = false;
        }

       

        private void pbCaturaCredencial_Click(object sender, EventArgs e)
        {
            pbVideoCredencial.Visible = true;
            pbCaturaCredencial.Visible = false;
            pImagenCredencial.BackColor = Color.LightGreen;
        }

        private void cmdNuevoRegistro_Click(object sender, EventArgs e)
        {
            lbNombreVisitante.Visible = false;
            pLbVisitante.Visible = false;

            txtNombreVisitante.Visible = true;
            pNombreVisitante.Visible = true;
            pNombreVisitante.BackColor = Color.Red;
            cmdNuevoRegistro.Visible = false;

            txtMotivo.Enabled = true;
            pMotivo.BackColor = Color.LightGreen;
            txtMotivo.Focus();

            pbCapturaRostro.Image = null;
            pbCaturaCredencial.Image = null;
            pbCapturaRostro.Visible = false;
            pbCaturaCredencial.Visible = false;
            pbVideoRostro.Visible = true;
            pbVideoCredencial.Visible = true;
        }
        //FIN EVENTOS CLIC
       
        //****
      
        //METODOS PROGRAMADOR
        public void desactivarBotones()
        {
            tsCmdAgregarHabitante.Enabled = false;
            tsCmdConsultaRegistro.Enabled = false;
            tsCmdAgregarGuardia.Enabled = false;
            tsLTipoUsuario.Text = "Guardia";
            lTurno.Visible = true;

            lNombre.Text = nombreUsuario.ToLower();
        }

        public void activarBotones()
        {
            tsCmdAgregarHabitante.Enabled = true;
            tsCmdConsultaRegistro.Enabled = true;
            tsCmdAgregarGuardia.Enabled = true;
            tsLTipoUsuario.Text = "Administrador";
            lTurno.Visible = false;
            lNombre.Text = nombreUsuario;
        }

        public string statusVisitante()
        {
            string status = "";
            string idVisitante = "";
            string sqlConsulta = "SELECT `idvisitante`,`status` FROM `Visitante` WHERE `nombre` = '" + lbNombreVisitante.Text + "'";

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(sqlConsulta);
            bd.adaptador.Fill(obtenerDatos, "Visitante");

            idVisitante = obtenerDatos.Tables[0].Rows[0]["idvisitante"].ToString();
            status = obtenerDatos.Tables[0].Rows[0]["status"].ToString();

            return status + "," + idVisitante;
        }

        public void cargarDatosVisitante(string idVisitante)
        {
            //int index = 0;
            string horaEntrada = "";
            string fechaIngreso = "";
            //string placas = "";
            string motivo = "";
            string idColono = "";
            string nombreHabitante = "";
            string sqlConsulta = "SELECT `horaEntrada`,`fechaIngreso`,`motivo`,`numeroCasa`,`idColono`" +
                                 "FROM `Registro` AS `CARGARDATOS` WHERE `idIngreso` = (SELECT MAX(idIngreso) FROM `Registro` WHERE `idVisitante` = '" + idVisitante + "')";

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(sqlConsulta);
            bd.adaptador.Fill(obtenerDatos, "CARGARDATOS");

            horaEntrada = obtenerDatos.Tables["CARGARDATOS"].Rows[0]["horaEntrada"].ToString();
            fechaIngreso = obtenerDatos.Tables["CARGARDATOS"].Rows[0]["fechaIngreso"].ToString();
            //placas = obtenerDatos.Tables["CARGARDATOS"].Rows[0]["placas"].ToString();
            motivo = obtenerDatos.Tables["CARGARDATOS"].Rows[0]["motivo"].ToString();
            idColono = obtenerDatos.Tables["CARGARDATOS"].Rows[0]["idColono"].ToString();

            lHI.Visible = true;
            lFI.Visible = true;
            lFechaIngreso.Visible = true;
            lHoraIngreso.Visible = true;

            lHoraIngreso.Text = Convert.ToDateTime(horaEntrada).ToShortTimeString();
            lFechaIngreso.Text = Convert.ToDateTime(fechaIngreso).ToString("dd - MMMM - yy").ToUpper();
            //txtPlacasVehiculo.Text = placas;

            txtMotivo.Enabled = false;
            txtMotivo.Text = motivo;
            txtPlacasVehiculo.Enabled = false;

            entra = 1;
            cbCasaQueVisita.DataSource = obtenerDatos.Tables["CARGARDATOS"];
            entra = 0;
            cbCasaQueVisita.DisplayMember = "numeroCasa";

            nombreHabitante = habitanteVisitado(idColono);

            lbHabitante.Visible = false;
            pHabitante.Visible = false;

            txtCargarHabitante.Visible = true;
            pCargarHabitante.Visible = true;

            txtCargarHabitante.Text = nombreHabitante;

            pMotivo.BackColor = Color.Red;
            lbNombreVisitante.Enabled = false;
            pLbVisitante.BackColor = Color.Red;

            cmdNuevoRegistro.Visible = false;

            tsCmdRegistrarIngreso.Enabled = false;
            tsCmdRegistrarSalida.Enabled = true;
            //index = cbRepresentante.FindString(nombreHabitante);

            //entra = 1;
            //cbRepresentante.SelectedIndex = index;
            //entra = 0;

          bd.cargarImagen(int.Parse(idVisitante),pbCapturaRostro,pbCaturaCredencial);
          pbCapturaRostro.Visible = true;
          pbCaturaCredencial.Visible = true;
          pbVideoRostro.Visible = false;
          pbVideoCredencial.Visible = false;
        }

        public string habitanteVisitado(string idColono)
        {
            string nombreColono = "";
            string sqlConsulta = "SELECT `nombre` FROM `Colono` WHERE `idcolono` = '" + idColono + "'";

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(sqlConsulta);
            bd.adaptador.Fill(obtenerDatos, "Colono");

            nombreColono = obtenerDatos.Tables[0].Rows[0][0].ToString();

            return nombreColono;
        }

        public void cargarImagen(string idVisitante)
        {
            //byte[] imagenRostro;
            //byte[] imagenCredencial;

            string sqlConsulta = "SELECT `fotoRostro`,`fotoCredencial` FROM `Imagen` WHERE `idVisitante` = '" + idVisitante + "'";
            

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(sqlConsulta);
            bd.adaptador.Fill(obtenerDatos, "Imagen");

            string rutaImagenRostro = obtenerDatos.Tables["Imagen"].Rows[0]["fotoRostro"].ToString();
            string rutaImagenCredencial = obtenerDatos.Tables["Imagen"].Rows[0]["fotoCredencial"].ToString();

           // MessageBox.Show(rutaImagenCredencial+rutaImagenRostro);

            pbCapturaRostro.Image =Image.FromFile(rutaImagenRostro);
            pbCaturaCredencial.Image = Image.FromFile(rutaImagenCredencial);

            //MemoryStream msRostro = new MemoryStream(imagenRostro);
            //MemoryStream msCredencial = new MemoryStream(imagenCredencial);

            pbCapturaRostro.Visible = true;
            pbCaturaCredencial.Visible = true;
            pbVideoRostro.Visible = false;
            pbVideoCredencial.Visible = false;

            //pbCapturaRostro.Image = Image.FromStream(msRostro);
            //pbCaturaCredencial.Image = Image.FromStream(msCredencial);
        }
        public void cargarImagenold(string idVisitante)
        {
            byte[] imagenRostro;
            byte[] imagenCredencial;

            string sqlConsulta = "SELECT `fotoRostro`,`fotoCredencial` FROM `Imagen` WHERE `idVisitante` = '" + idVisitante + "'";

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();



            bd.adaptador = bd.consultaAdaptador(sqlConsulta);
            bd.adaptador.Fill(obtenerDatos, "Imagen");

            imagenRostro = (byte[])obtenerDatos.Tables["Imagen"].Rows[0]["fotoRostro"];
            imagenCredencial = (byte[])obtenerDatos.Tables["Imagen"].Rows[0]["fotoCredencial"];

            MemoryStream msRostro = new MemoryStream(imagenRostro);
            MemoryStream msCredencial = new MemoryStream(imagenCredencial);

            pbCapturaRostro.Visible = true;
            pbCaturaCredencial.Visible = true;
            pbVideoRostro.Visible = false;
            pbVideoCredencial.Visible = false;

            pbCapturaRostro.Image = Image.FromStream(msRostro);
            pbCaturaCredencial.Image = Image.FromStream(msCredencial);
        }

        public int obtenerId()
        {
            int id = 0;
            string sqlConsulta = "SELECT MAX(idvisitante) FROM Visitante";

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(sqlConsulta);
            bd.adaptador.Fill(obtenerDatos, "Visitante");

            string recibeDato = obtenerDatos.Tables[0].Rows[0][0].ToString();

            id = Convert.ToInt32(recibeDato);
            return id;
        }

        public int visitanteExiste()
        {
            int id = 0;
            string sqlConsulta = "SELECT `idvisitante` FROM `Visitante`  WHERE `nombre` = '" + lbNombreVisitante.Text + "'";

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(sqlConsulta);
            bd.adaptador.Fill(obtenerDatos, "Visitante");

            try
            {
                string recibeDato = obtenerDatos.Tables[0].Rows[0][0].ToString();
                id = Convert.ToInt32(recibeDato);
                return id;
            }
            catch (IndexOutOfRangeException)
            {
                return id;
            }
        }

        public bool placaExiste(int idVisitante)
        {
            string placa = "";
            string sqlConsulta = "SELECT placa FROM Placas WHERE idVisitante = " + idVisitante + " and placa = '" + txtPlacasVehiculo.Text + "'";

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(sqlConsulta);
            bd.adaptador.Fill(obtenerDatos, "Placas");

            try
            {
                placa = obtenerDatos.Tables[0].Rows[0][0].ToString();

                if (placa != "")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (IndexOutOfRangeException)
            {

                return false;
            }
        }

        private void limpiar()
        {

            //cbNombre.Text = "";
            cbCasaQueVisita.Text = "";

            txtNombreVisitante.Text = "";
            txtPlacasVehiculo.Text = "";
            txtMotivo.Text = "";

            lbHabitante.DataSource = null;

            pbCapturaRostro.Visible = false;
            pbVideoRostro.Visible = true;
            pbCaturaCredencial.Visible = false;
            pbVideoCredencial.Visible = true;

            lHI.Visible = false;
            lFI.Visible = false;
            lFechaIngreso.Visible = false;
            lHoraIngreso.Visible = false;

            pHabitante.Visible = true;
            pCargarHabitante.Visible = false;

            txtCargarHabitante.Visible = false;

            txtPlacasVehiculo.Enabled = true;

            lbHabitante.Enabled = false;
            lbHabitante.Visible = true;

            txtMotivo.Enabled = false;
            txtNombreVisitante.Enabled = false;

            pbCapturaRostro.Image = null;
            pbVideoCredencial.Image = null;

            lTelefono.Text = "";

            cmdNuevoRegistro.Visible = false;

            pHabitante.BackColor = Color.Red;
            pMotivo.BackColor = Color.Red;
            pNombreVisitante.BackColor = Color.Red;
            pLbVisitante.BackColor = Color.Red;
            pHabitante.BackColor = Color.Red;
            pImagenRostro.BackColor = Color.Red;
            pImagenCredencial.BackColor = Color.Red;

            tsCmdRegistrarIngreso.Enabled = true;
            tsCmdRegistrarSalida.Enabled = false;

            bd.llenarComboBox(cbCasaQueVisita,sqlNumeroCasa, "numero", "Casa", ref entra);


        }
        //FIN METODOS 

        //*****

        //MENU TOOLSTRIP
        private void tsCmdAgregarHabitante_Click(object sender, EventArgs e)
        {
            AgregarHabitante habitante = new AgregarHabitante();

            habitante.Show();
        }

        private void tsCmdConsultaRegistro_Click(object sender, EventArgs e)
        {
            ConsultarRegistros consultarRegistros = new ConsultarRegistros();

            consultarRegistros.Show();
        }

        private void tsCmdAgregarGuardia_Click(object sender, EventArgs e)
        {
            AgregarGuardia agregarGuardia = new AgregarGuardia();

            agregarGuardia.Show();
        }

        private void tsCmdLimpiarDatos_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void tsCmdActualizar_Click(object sender, EventArgs e)
        {
            bd.llenarComboBox(cbCasaQueVisita, sqlNumeroCasa, "numero", "Casa", ref entra);

        }

        private void tsCmdConfiguracion_Click(object sender, EventArgs e)
        {
            cbCamaraCredencial.Visible = true;
            cbCamaraRostro.Visible = true;
            lCamaraCredencial.Visible = true;
            lCamaraRostro.Visible = true;
            
        }

        private void tsCmdRegistrarIngreso_Click(object sender, EventArgs e)
        {
            if (/*lbNombreVisitante.Text != "" || txtNombreVisitante.Text != "" &&*/ txtPlacasVehiculo.Text != "" && txtMotivo.Text != "" && cbCasaQueVisita.SelectedIndex != -1 && lbHabitante.Text != "")
            {
                if (pbVideoRostro.Visible == false)
                {
                    int idVisitante = 0;
                    string nombreVisitante = "";
                    string placas = txtPlacasVehiculo.Text;
                    string numeroCasa = cbCasaQueVisita.Text;
                    string habitante = lbHabitante.Text;
                    string fecha = DateTime.Today.ToShortDateString();
                    string hora = DateTime.Now.ToLongTimeString();
                    string motivo = txtMotivo.Text;
                    string sqlInsetarIngreso = "";
                    string sqlUpdateVisitante = "";
                    string sqliInsertarPlaca = "";

                    string idColono = "(SELECT `idcolono` FROM `Colono` WHERE `nombre` = '" + habitante + "' AND `numero` = " + numeroCasa + ")";

                    //string sqlInsertarVisitante = "INSERT INTO `Visitante`(`nombre`,`status`)" +
                    //                                             " VALUES ('" + nombreVisitante + "', 1);";
                    //id = visitanteExiste();


                    if (txtNombreVisitante.Enabled != true && txtNombreVisitante.Visible != true)
                    {
                        idVisitante = visitanteExiste();

                        sqliInsertarPlaca = "INSERT INTO `Placas`(`idVisitante`,`placa`) VALUES (" + idVisitante + ",'" + placas + "')";

                        sqlUpdateVisitante = "UPDATE `Visitante` SET `status`= 1 WHERE `idvisitante`= " + idVisitante + "";

                        sqlInsetarIngreso = "INSERT INTO `Registro`(`horaEntrada`,`horaSalida`,`fechaIngreso`,`idVisitante`,`motivo`,`numeroCasa`,`idColono`,`idGuardia`)" +
                                                           "VALUES ('" + hora + "', NULL, '" + fecha + "'," + idVisitante + ",  '" + motivo + "', " + numeroCasa + "," + idColono + "," + Convert.ToInt32(idGuardia) + ")";

                        if (bd.insertar(sqlInsetarIngreso) && bd.insertar(sqlUpdateVisitante))
                        {
                            MessageBox.Show("Registro ingresado correctamente");
                        }
                        else
                        {
                            MessageBox.Show("Error al intetar insertar datos");
                        }

                        if (!placaExiste(idVisitante))
                        {
                            bd.insertar(sqliInsertarPlaca);
                            MessageBox.Show("placas insertadas correctamente para nuevo usuario");
                        }
                        //else
                        //{
                        //    bd.insertar(sqliInsertarPlaca);
                        //    MessageBox.Show("El registro de placas ya existe se registro a un nuevo visitante");
                        //}

                        //respuestaInsertarImagen = bd.guradarImagen(pbCapturaRostro, id);

                        //string sqlInsetar = "INSERT INTO `Registro`(`nombre`, `motivo`,`placas`,`horaentrada`,`horasalida`,`fechadeingreso`,`numero`,`idColono`,`status`) " +
                        //                      "VALUES ('" + nombreVisitante + "', NULL,'" + placas + "', '" + hora + "', NULL,'" + fecha + "'," + numeroCasa + "," + idColono + ", 1)";
                    }
                    else
                    {
                        nombreVisitante = txtNombreVisitante.Text;

                        string sqlInsertarVisitante = "INSERT INTO `Visitante`(`nombre`,`status`)" +
                                                                 " VALUES ('" + nombreVisitante + "', 1);";
                        bd.insertar(sqlInsertarVisitante);

                        idVisitante = obtenerId();

                        sqliInsertarPlaca = "INSERT INTO `Placas`(`idVisitante`,`placa`) VALUES (" + idVisitante + ",'" + placas + "')";

                        sqlInsetarIngreso = "INSERT INTO `Registro`(`horaEntrada`,`horaSalida`,`fechaIngreso`,`idVisitante`,`motivo`,`numeroCasa`,`idColono`,`idGuardia`) " +
                                                           "VALUES ('" + hora + "', NULL, '" + fecha + "'," + idVisitante + ",  '" + motivo + "', " + numeroCasa + "," + idColono + "," + Convert.ToInt32(idGuardia) + ")";

                        if (bd.insertar(sqlInsetarIngreso)  && bd.guradarImagenArchivo(pbCapturaRostro, pbCaturaCredencial, idVisitante,txtNombreVisitante.Text))
                        {
                            MessageBox.Show("Registro ingresado correctamente para nuevo usuario");
                        }
                        else
                        {
                            MessageBox.Show("Error al intetar insertar datos");
                        }

                        if (!placaExiste(idVisitante))
                        {
                            bd.insertar(sqliInsertarPlaca);
                            MessageBox.Show("placas insertadas correctamente");
                        }
                        //else
                        //{
                        //    bd.insertar(sqliInsertarPlaca);
                        //    MessageBox.Show("El registro de placas ya existe se registro a un nuevo visitante");
                        //}
                    }
                    limpiar();
                    //bd.llenarComboBox(cbNombre, "nombre", "Visitante", "", "nombre", false, ref entra);
                    //cbCasaQueVisita.DataSource = null;

                    bd.llenarComboBox(cbCasaQueVisita, sqlNumeroCasa, "numero", "Casa", ref entra);

                }//if captura credencial 
                else
                {
                    MessageBox.Show("Favor de hacer la captura del rostro y la credencial");
                }
            }
            else
            {
                MessageBox.Show("Favor de introducir todos los datos");
            }
        }

        private void tsCmdRegistrarSalida_Click(object sender, EventArgs e)
        {
            bool respuestaActualizaRegistro = false;
            bool respuestaActualizaVisitante = false;
            string statusMasIdVisitante = statusVisitante();
            string[] splitStatusMasIdVisitante = statusMasIdVisitante.Split(new char[] { ',' });
            int status = Convert.ToInt32(splitStatusMasIdVisitante[0]);
            string idVisitante = splitStatusMasIdVisitante[1];
            string fechaSalida = DateTime.Today.ToShortDateString();
            string datos = "`horaSalida` = '" + lHora.Text + "', fechaSalida = '" + fechaSalida + "'";
            string datosStatus = "`status`= 0";

            string condicionActualizaRegistro = "`idIngreso`= (SELECT MAX(idIngreso) FROM `Registro` WHERE `idVisitante` = '" + idVisitante + "')";
            string condicionActualizaVisitante = "`idvisitante`= '" + idVisitante + "'";

            respuestaActualizaRegistro = bd.actualizar("`Registro`", datos, condicionActualizaRegistro);

            respuestaActualizaVisitante = bd.actualizar("`Visitante`", datosStatus, condicionActualizaVisitante);

            if (respuestaActualizaRegistro && respuestaActualizaVisitante)
            {
                MessageBox.Show("La salidad se registro a las " + lHora.Text);
            }
            else
            {
                MessageBox.Show("Error al registrar la salida");
            }

            limpiar();

            cbCasaQueVisita.DataSource = null;

            bd.llenarComboBox(cbCasaQueVisita, sqlNumeroCasa, "numero", "Casa", ref entra);

            lHoraIngreso.Visible = false;
            lFechaIngreso.Visible = false;
            lHI.Visible = false;
            lFI.Visible = false;
            tsCmdRegistrarIngreso.Enabled = true;
            tsCmdRegistrarSalida.Enabled = false;
        }

        private void tsCmdCerrarSesion_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Restart();
        }

        //FIN MENU

        private void cambioCamaras_Click(object sender, EventArgs e)
        {
            if (banderaCamara)
            {
                camaraCredencial.detenerVideo();
                camaraRostro.detenerVideo();

                camaraCredencial.cargarVideo(idCamaraRostro);
                camaraRostro.cargarVideo(idCamaraCredencial);
                banderaCamara = false;
            }
            else
            {
                camaraCredencial.detenerVideo();
                camaraRostro.detenerVideo();

                camaraCredencial.cargarVideo(idCamaraCredencial);
                camaraRostro.cargarVideo(idCamaraRostro);
                banderaCamara = true;
            }

            

        }

       

        private void pbVideoRostro_Click(object sender, EventArgs e)
        {
            camaraRostro.capturarImagen();

            pbVideoRostro.Visible = false;
            pbCapturaRostro.Visible = true;
            pImagenRostro.BackColor = Color.Blue;
            pImagenCredencial.BackColor = Color.LightGreen;
            pbVideoCredencial.Enabled = true;
            //cmdCapturaRostro.Visible = false;
            //cmdNuevaFotoRostro.Visible = true;
            //cmdCapturarCredencial.Enabled = true;
        }

        private void pbVideoCredencial_Click(object sender, EventArgs e)
        {
            camaraCredencial.capturarImagen();

            pbVideoCredencial.Visible = false;
            pbCaturaCredencial.Visible = true;
            pImagenCredencial.BackColor = Color.Blue;
        }
        Notificacion[] notificaciones;
        private void alertaIntencion_Tick(object sender, EventArgs e)
        {

            if (DateTime.Now.ToLongTimeString().CompareTo("01:00:00 p.m.") > 0)
            {
                notificaciones = bd.manejoAlertas(tsbNotificacion);
                if (notificaciones != null)
                {
                    listNotificaciones.Items.Clear();
                    for (int i = 0; i < notificaciones.Length; i++)
                    {
                        listNotificaciones.Items.Add(notificaciones[i].Accionador + ": \nEl Usuario " + notificaciones[i].VisitanteDetalles.NombreVisitante + " no ha registrado Salida");
                        
                    }
                }
            }
        }

        private void tsbNotificacion_Click(object sender, EventArgs e)
        {
            listNotificaciones.Visible = true;
            listNotificaciones.Focus();
        }

        

        private void listNotificaciones_SelectedIndexChanged(object sender, EventArgs e)
        {

            txtPlacasVehiculo.Text = notificaciones[listNotificaciones.SelectedIndex].VisitanteDetalles.PlacasVisitante;
            cargarDatosVisitante(notificaciones[listNotificaciones.SelectedIndex].VisitanteDetalles.IdVisitante.ToString());
        
        }

        private void listNotificaciones_Leave(object sender, EventArgs e)
        {
            listNotificaciones.Visible = false;
        }
    }
}

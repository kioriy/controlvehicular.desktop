﻿namespace ControlDeIngresoVehicular
{
    partial class AgregarHabitante
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AgregarHabitante));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbNumero = new System.Windows.Forms.ComboBox();
            this.cmdModificarHabitante = new System.Windows.Forms.Button();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.txtCalle = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmdAgregarCasa = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.gbDatosHabitante = new System.Windows.Forms.GroupBox();
            this.cmdAgregarHabitante = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.nuevoRegistroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdModificarCasa = new System.Windows.Forms.Button();
            this.cmdTerminar = new System.Windows.Forms.Button();
            this.dgvHabitantes = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.gbDatosHabitante.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHabitantes)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbNumero);
            this.groupBox1.Controls.Add(this.cmdModificarHabitante);
            this.groupBox1.Controls.Add(this.txtTelefono);
            this.groupBox1.Controls.Add(this.txtCalle);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(321, 144);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos casa";
            // 
            // cbNumero
            // 
            this.cbNumero.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbNumero.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbNumero.FormattingEnabled = true;
            this.cbNumero.Location = new System.Drawing.Point(64, 28);
            this.cbNumero.Name = "cbNumero";
            this.cbNumero.Size = new System.Drawing.Size(65, 21);
            this.cbNumero.TabIndex = 1;
            this.cbNumero.SelectedIndexChanged += new System.EventHandler(this.cbNumero_SelectedIndexChanged);
            // 
            // cmdModificarHabitante
            // 
            this.cmdModificarHabitante.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdModificarHabitante.ForeColor = System.Drawing.Color.Red;
            this.cmdModificarHabitante.Location = new System.Drawing.Point(179, 23);
            this.cmdModificarHabitante.Name = "cmdModificarHabitante";
            this.cmdModificarHabitante.Size = new System.Drawing.Size(136, 31);
            this.cmdModificarHabitante.TabIndex = 9;
            this.cmdModificarHabitante.Text = "Mostrar habitantes";
            this.cmdModificarHabitante.UseVisualStyleBackColor = true;
            this.cmdModificarHabitante.Visible = false;
            this.cmdModificarHabitante.Click += new System.EventHandler(this.cmdModificarHabitante_Click);
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(64, 108);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(128, 20);
            this.txtTelefono.TabIndex = 3;
            // 
            // txtCalle
            // 
            this.txtCalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCalle.Location = new System.Drawing.Point(64, 66);
            this.txtCalle.Name = "txtCalle";
            this.txtCalle.Size = new System.Drawing.Size(251, 20);
            this.txtCalle.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Telefono:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Calle:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Numero:";
            // 
            // cmdAgregarCasa
            // 
            this.cmdAgregarCasa.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAgregarCasa.Location = new System.Drawing.Point(109, 185);
            this.cmdAgregarCasa.Name = "cmdAgregarCasa";
            this.cmdAgregarCasa.Size = new System.Drawing.Size(115, 36);
            this.cmdAgregarCasa.TabIndex = 4;
            this.cmdAgregarCasa.Text = "Agregar casa";
            this.cmdAgregarCasa.UseVisualStyleBackColor = true;
            this.cmdAgregarCasa.Click += new System.EventHandler(this.cmdAgregarCasa_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 70);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Nombre:";
            // 
            // txtNombre
            // 
            this.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombre.Location = new System.Drawing.Point(64, 66);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(251, 20);
            this.txtNombre.TabIndex = 5;
            // 
            // gbDatosHabitante
            // 
            this.gbDatosHabitante.Controls.Add(this.txtNombre);
            this.gbDatosHabitante.Controls.Add(this.label6);
            this.gbDatosHabitante.Location = new System.Drawing.Point(339, 35);
            this.gbDatosHabitante.Name = "gbDatosHabitante";
            this.gbDatosHabitante.Size = new System.Drawing.Size(321, 144);
            this.gbDatosHabitante.TabIndex = 5;
            this.gbDatosHabitante.TabStop = false;
            this.gbDatosHabitante.Text = "Datos habitante";
            // 
            // cmdAgregarHabitante
            // 
            this.cmdAgregarHabitante.Enabled = false;
            this.cmdAgregarHabitante.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAgregarHabitante.Location = new System.Drawing.Point(420, 185);
            this.cmdAgregarHabitante.Name = "cmdAgregarHabitante";
            this.cmdAgregarHabitante.Size = new System.Drawing.Size(159, 36);
            this.cmdAgregarHabitante.TabIndex = 6;
            this.cmdAgregarHabitante.Text = "Agregar habitante";
            this.cmdAgregarHabitante.UseVisualStyleBackColor = true;
            this.cmdAgregarHabitante.Click += new System.EventHandler(this.cmdAgregarHabitante_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevoRegistroToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(684, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // nuevoRegistroToolStripMenuItem
            // 
            this.nuevoRegistroToolStripMenuItem.Name = "nuevoRegistroToolStripMenuItem";
            this.nuevoRegistroToolStripMenuItem.Size = new System.Drawing.Size(97, 20);
            this.nuevoRegistroToolStripMenuItem.Text = "Nuevo registro";
            this.nuevoRegistroToolStripMenuItem.Click += new System.EventHandler(this.nuevoRegistroToolStripMenuItem_Click);
            // 
            // cmdModificarCasa
            // 
            this.cmdModificarCasa.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdModificarCasa.ForeColor = System.Drawing.Color.MediumBlue;
            this.cmdModificarCasa.Location = new System.Drawing.Point(74, 185);
            this.cmdModificarCasa.Name = "cmdModificarCasa";
            this.cmdModificarCasa.Size = new System.Drawing.Size(184, 36);
            this.cmdModificarCasa.TabIndex = 8;
            this.cmdModificarCasa.Text = "Modificar datos casa";
            this.cmdModificarCasa.UseVisualStyleBackColor = true;
            this.cmdModificarCasa.Visible = false;
            this.cmdModificarCasa.Click += new System.EventHandler(this.cmdModificarCasa_Click);
            // 
            // cmdTerminar
            // 
            this.cmdTerminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdTerminar.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.cmdTerminar.Location = new System.Drawing.Point(420, 185);
            this.cmdTerminar.Name = "cmdTerminar";
            this.cmdTerminar.Size = new System.Drawing.Size(159, 36);
            this.cmdTerminar.TabIndex = 8;
            this.cmdTerminar.Text = "Terminar !!";
            this.cmdTerminar.UseVisualStyleBackColor = true;
            this.cmdTerminar.Visible = false;
            this.cmdTerminar.Click += new System.EventHandler(this.cmdTerminar_Click);
            // 
            // dgvHabitantes
            // 
            this.dgvHabitantes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvHabitantes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHabitantes.Location = new System.Drawing.Point(339, 35);
            this.dgvHabitantes.Name = "dgvHabitantes";
            this.dgvHabitantes.Size = new System.Drawing.Size(335, 146);
            this.dgvHabitantes.TabIndex = 5;
            this.dgvHabitantes.Visible = false;
            this.dgvHabitantes.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvHabitantes_CellEndEdit);
            // 
            // AgregarHabitante
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 230);
            this.Controls.Add(this.gbDatosHabitante);
            this.Controls.Add(this.cmdAgregarCasa);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.dgvHabitantes);
            this.Controls.Add(this.cmdTerminar);
            this.Controls.Add(this.cmdAgregarHabitante);
            this.Controls.Add(this.cmdModificarCasa);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "AgregarHabitante";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agregar habitante";
            this.Load += new System.EventHandler(this.AgregarHabitante_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbDatosHabitante.ResumeLayout(false);
            this.gbDatosHabitante.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHabitantes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.TextBox txtCalle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cmdAgregarCasa;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.GroupBox gbDatosHabitante;
        private System.Windows.Forms.Button cmdAgregarHabitante;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem nuevoRegistroToolStripMenuItem;
        private System.Windows.Forms.ComboBox cbNumero;
        private System.Windows.Forms.Button cmdModificarHabitante;
        private System.Windows.Forms.Button cmdModificarCasa;
        private System.Windows.Forms.Button cmdTerminar;
        private System.Windows.Forms.DataGridView dgvHabitantes;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlDeIngresoVehicular
{
    
    class Class
    {
       
    }
     
    class Registro    
    {

        int idVisitante;
        string fechaIngreso;
        string horaEntrada;
        string motivoVisitante;
        int casaVisita;
        int idColono;
        string placasVisitante;
        string nombreVisitante;

        

        public bool cargaRegistroVisitante(int idRegistro)
        {
            BD dataBase = new BD();
            try
            {

                dataBase.conexionAbrir();
                string sqlConsulta = "SELECT R.idVisitante, R.fechaIngreso, R.horaEntrada, R.motivo, R.numeroCasa, R.idColono,V.nombre, P.placa FROM Registro as R INNER JOIN Visitante as V on R.idVisitante=V.idvisitante INNER JOIN Placas as P on R.idVisitante=P.idVisitante WHERE idIngreso='"+idRegistro+"'";
                
                //MessageBox.Show(sqlConsulta);
            
                dataBase.sqliteCMD(sqlConsulta);
                dataBase.Reader.Read();
            
                idVisitante =int.Parse(dataBase.Reader[0].ToString());
                fechaIngreso = Convert.ToString(dataBase.Reader[1]);
                horaEntrada = Convert.ToString(dataBase.Reader[2]);
                motivoVisitante = Convert.ToString(dataBase.Reader[3]);
                casaVisita = int.Parse(dataBase.Reader[4].ToString());
                idColono = int.Parse(dataBase.Reader[5].ToString());
                nombreVisitante = dataBase.Reader[6].ToString();
                placasVisitante = dataBase.Reader[7].ToString();

                dataBase.conexionCerrar();
                return true;
            
            }
            catch(Exception ex)
            {
                return false;

            }

        }
        public int IdVisitante
        {
            get { return idVisitante; }
            set { idVisitante = value; }
        }
        public string FechaIngreso
        {
            get { return fechaIngreso; }
            set { fechaIngreso = value; }
        }
        
        public string HoraEntrada
        {
            get { return horaEntrada; }
            set { horaEntrada = value; }
        }
        public string MotivoVisitante
        {
            get { return motivoVisitante; }
            set { motivoVisitante = value; }
        }
        public int CasaVisita
        {
            get { return casaVisita; }
            set { casaVisita = value; }
        }
        public int IdColono
        {
            get { return idColono; }
            set { idColono = value; }
        }
        public string PlacasVisitante
        {
            get { return placasVisitante; }
            set { placasVisitante = value; }
        }

        public string NombreVisitante
        {
            get { return nombreVisitante; }
            set { nombreVisitante = value; }
        }
        
        
    }
    
}

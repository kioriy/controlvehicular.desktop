﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text;
using System.Threading.Tasks;
using AForge.Video;
using AForge.Video.DirectShow;
using System.Drawing;

namespace ControlDeIngresoVehicular
{
    class Video
    {
        private FilterInfoCollection dispositivos = new FilterInfoCollection(FilterCategory.VideoInputDevice);
        private VideoCaptureDevice fuenteDeVideo;
        private System.Windows.Forms.PictureBox pbImagenRostro;
        private System.Windows.Forms.PictureBox pbCapturaRostro;

        public Video(System.Windows.Forms.PictureBox pbImagenRostro, System.Windows.Forms.PictureBox pbCapturaRostro)
        {
            this.pbImagenRostro = pbImagenRostro;
            this.pbCapturaRostro = pbCapturaRostro;
        }

        public bool existenDispositivos() 
        {
            if (dispositivos.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void cargarComboBox(System.Windows.Forms.ComboBox llenarComboBox) 
        {
            dispositivos = new FilterInfoCollection(FilterCategory.VideoInputDevice);
           
            foreach(FilterInfo cargaDispositivos in dispositivos)
            {
                llenarComboBox.Items.Add(cargaDispositivos);
            }
        }

        public string cargarVideo(int numeroCamara)
        {
            if (existenDispositivos())
            {
                fuenteDeVideo = new VideoCaptureDevice(dispositivos[numeroCamara].MonikerString);

                //vspCamaraRostro.VideoSource = fuenteDeVideo;
                fuenteDeVideo.NewFrame += new NewFrameEventHandler(cargaImagen);
                fuenteDeVideo.Start();

                return "0";
            }
            else 
            {
                return "No se encontro algun dispositivo de video";
            }

        }
        public string cambiaVideo(int numeroCamara)
        {
            if (existenDispositivos())
            {
                fuenteDeVideo = new VideoCaptureDevice(dispositivos[numeroCamara].MonikerString);

                //vspCamaraRostro.VideoSource = fuenteDeVideo;
                fuenteDeVideo.NewFrame += new NewFrameEventHandler(cargaImagen);
                fuenteDeVideo.Start();

                return "0";
            }
            else
            {
                return "No se encontro algun dispositivo de video";
            }

        }

        public void cargaImagen(object sender, NewFrameEventArgs eventArgs) 
        {
            Bitmap imagen = (Bitmap)eventArgs.Frame.Clone();
            pbImagenRostro.Image = imagen;
        }

        public void capturarImagen()
        {
            if (fuenteDeVideo != null) 
            {
                if (fuenteDeVideo.IsRunning) 
                {
                    pbCapturaRostro.Image = pbImagenRostro.Image;
                }
            }
        }

        public void detenerVideo() 
        {
            if (fuenteDeVideo != null)
            {
                if (fuenteDeVideo.IsRunning)
                {
                    fuenteDeVideo.SignalToStop();
                    fuenteDeVideo = null;
                }
            }
            //Dispose();
            //Close();
        }
    }
}

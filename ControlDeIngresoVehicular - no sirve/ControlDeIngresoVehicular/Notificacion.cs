﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlDeIngresoVehicular
{
    class Notificacion
    {

        string descripcion;
        DateTime hora;
        DateTime fecha;
        string  accionador;
        Registro visitanteDetalles;

        

        //static  ArrayList recolectorNotificaciones = new ArrayList();

        public void guardarNotificacion()
        {   

            

        }
        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }
        public DateTime Hora
        {
            get { return hora; }
            set { hora = value; }
        }
        public DateTime Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }
        public string Accionador
        {
            get { return accionador; }
            set { accionador = value; }
        }
        internal Registro VisitanteDetalles
        {
            get { return visitanteDetalles; }
            set { visitanteDetalles = value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.Configuration;
using System.IO;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Drawing;
using ControlDeIngresoVehicular;

namespace ControlDeIngresoVehicular
{
    class BD
    {
        string parametrosConexion = System.Configuration.ConfigurationManager.ConnectionStrings["db"].ConnectionString;

        SQLiteConnection conexionBD;
        SQLiteCommand queryCommand;
        public SQLiteDataAdapter adaptador;
        SQLiteDataReader reader;

        
        SQLiteParameter picture;
        SQLiteParameter picture2;
        DataSet obtenerDatos;
        
        public bool conexionAbrir()
        {
            conexionBD = new SQLiteConnection(parametrosConexion);
            ConnectionState estadoConexion;
           
            try
            {
                conexionBD.Open();
                estadoConexion = conexionBD.State;
                return true;
            }
            catch(SQLiteException)
            {
                return false;
            }
        }
        public SQLiteDataReader sqliteCMD(string sqliteConsulta)
        {
            queryCommand = new SQLiteCommand(sqliteConsulta,conexionBD);
            reader = queryCommand.ExecuteReader();
            
            return reader;
        }

        public bool insertar(string sqliteInsertar) 
        {
            int filasAfectadas = 0;
            conexionAbrir();

            queryCommand = new SQLiteCommand(sqliteInsertar, conexionBD);
             filasAfectadas = queryCommand.ExecuteNonQuery();
            
            conexionCerrar();
            
            if (filasAfectadas > 0) 
            {
                return true;
            }
            else 
            {
                return false;
            }
        }

        public SQLiteDataAdapter consultaAdaptador(string queryConsulta) 
        {
            conexionAbrir();

            adaptador = new SQLiteDataAdapter(queryConsulta, conexionBD);

            conexionCerrar();

            return adaptador;
        }

        public void llenarComboBox(System.Windows.Forms.ComboBox llenarCombo, string sqlLlenarComboBox, string columna, string tabla, /*string condicion, string orderBy, bool conCondicion,*/ref int entra) 
        {
            //string sqlLlenarComboBox = "";

            //if (conCondicion)
            //{
            //    sqlLlenarComboBox  = "SELECT " + columna + " FROM " + tabla + " WHERE " + condicion + " ORDER BY " + orderBy + " ASC";
            //}
            //else
            //{
            //    sqlLlenarComboBox = "SELECT " + columna + " FROM " + tabla + " ORDER BY " + orderBy + " ASC";
            //}

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();
            adaptador = consultaAdaptador(sqlLlenarComboBox);

            adaptador.Fill(obtenerDatos,tabla);

            entra = 1;
            llenarCombo.DataSource = obtenerDatos.Tables[tabla];
            llenarCombo.DisplayMember = columna;
            llenarCombo.SelectedIndex = -1;
            entra = 0;
        }

        public void llenarListBox(System.Windows.Forms.ListBox llenarlista, string columna, string tabla, string condicion, string orderBy, bool conCondicion, ref int entra)
        {
            string sqlLlenarComboBox = "";

            if (conCondicion)
            {
                sqlLlenarComboBox = "SELECT " + columna + " FROM " + tabla + " WHERE " + condicion + " ORDER BY " + orderBy + " ASC";
            }
            else
            {
                sqlLlenarComboBox = "SELECT " + columna + " FROM " + tabla + " ORDER BY " + orderBy + " ASC";
            }

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();
            adaptador = consultaAdaptador(sqlLlenarComboBox);

            adaptador.Fill(obtenerDatos, tabla);

            entra = 1;
            llenarlista.DataSource = obtenerDatos.Tables[tabla];
            llenarlista.DisplayMember = columna;
            //llenarCombo.SelectedIndex = -1;
            entra = 0;
        }

        public void conexionCerrar()
        {
           if (conexionBD.State == ConnectionState.Open)
           {
               conexionBD.Close();
           }
        }

        public bool guradarImagen(System.Windows.Forms.PictureBox pbImagen, System.Windows.Forms.PictureBox pbImagen2, int id) 
        {
            if (pbImagen.Image != null) 
            {
                int filasAfectadas = 0;

                picture = new SQLiteParameter("@picture", SqlDbType.Image);
                picture2 = new SQLiteParameter("@picture2", SqlDbType.Image);

                MemoryStream ms = new MemoryStream();
                MemoryStream ms2 = new MemoryStream();

                pbImagen.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                pbImagen2.Image.Save(ms2, System.Drawing.Imaging.ImageFormat.Bmp);
                

                byte[] imagenByte = ms.GetBuffer();
                byte[] imagenByte2 = ms2.GetBuffer();

                ms.Close();
                conexionAbrir();
                queryCommand = new SQLiteCommand(conexionBD);
                queryCommand.Parameters.Clear();
                queryCommand.Parameters.AddWithValue("@picture", imagenByte);
                queryCommand.Parameters.AddWithValue("@picture2", imagenByte2);
                queryCommand.CommandText = /*"UPDATE `Visitante` SET `fotorostro` = (@picture) WHERE `idvisitante` = " + idVisitante + "";*/"INSERT INTO `Imagen`(`idVisitante`,`fotoRostro`,`fotoCredencial`) VALUES (" + id + ", @picture, @picture2)";

                filasAfectadas =  queryCommand.ExecuteNonQuery();
                conexionCerrar();

                if (filasAfectadas > 0) 
                {
                    return true;
                }
                else 
                {
                    return false;
                }
            }
            return false;
        }
        public bool guradarImagenArchivo(System.Windows.Forms.PictureBox pbImagen, System.Windows.Forms.PictureBox pbImagen2, int id,string nombreVisitante)
        {
            if (pbImagen.Image != null && pbImagen2.Image != null)
            {
                int filasAfectadas = 0;
                
                DateTime Hoy = DateTime.Today;
                string fecha_actual = Hoy.ToString("dd-MM-yyyy");
               
                string rutaImagen ="C:\\ControlVehicular\\imagenes\\"+fecha_actual+"\\"+id+"_"+nombreVisitante.Replace(' ','_');

                if(!Directory.Exists(rutaImagen))
                {    
                    Directory.CreateDirectory(rutaImagen);
                }

                string rutaImagenRostro=Path.Combine(rutaImagen, id + nombreVisitante + "Rostro.jpg");
                string rutaImagenCredencial=Path.Combine(rutaImagen, id + nombreVisitante + "Credencial.jpg");
                pbImagen.Image.Save(rutaImagenRostro, ImageFormat.Jpeg);
                pbImagen2.Image.Save(rutaImagenCredencial, ImageFormat.Jpeg);
                conexionAbrir();

                queryCommand = new SQLiteCommand(conexionBD);
                string query="INSERT INTO `Imagen`(`idVisitante`,`fotoRostro`,`fotoCredencial`) VALUES (" + id + ",'"+rutaImagenRostro+"', '"+rutaImagenCredencial+"')";
                //MessageBox.Show(query);
                queryCommand.CommandText = query;
                filasAfectadas = queryCommand.ExecuteNonQuery();
                conexionCerrar();
                
                /*
                picture = new SQLiteParameter("@picture", SqlDbType.Image);
                picture2 = new SQLiteParameter("@picture2", SqlDbType.Image);
                MemoryStream ms = new MemoryStream();
                MemoryStream ms2 = new MemoryStream();

                pbImagen.Image.Save(rutaImagen);
                pbImagen2.Image.Save(ms2, System.Drawing.Imaging.ImageFormat.Bmp);

                byte[] imagenByte = ms.GetBuffer();
                byte[] imagenByte2 = ms2.GetBuffer();

                ms.Close();
                
                conexionAbrir();
                
                queryCommand = new SQLiteCommand(conexionBD);
                queryCommand.Parameters.Clear();
                queryCommand.Parameters.AddWithValue("@picture", imagenByte);
                queryCommand.Parameters.AddWithValue("@picture2", imagenByte2);
                queryCommand.CommandText = /*"UPDATE `Visitante` SET `fotorostro` = (@picture) WHERE `idvisitante` = " + idVisitante + "";   comentado "INSERT INTO `Imagen`(`idVisitante`,`fotoRostro`,`fotoCredencial`) VALUES (" + id + ", @picture, @picture2)";

                filasAfectadas = queryCommand.ExecuteNonQuery();
                conexionCerrar();
                */

                return true;
                
            }
            return false;
        }

        public bool actualizar(string tabla, string datos, string condicion) 
        {
            conexionAbrir();

            int filasAfectadas = 0;
            string queryActualizar = "UPDATE " + tabla + " SET " + datos + " WHERE " + condicion;

            queryCommand = new SQLiteCommand(queryActualizar, conexionBD);

            filasAfectadas = queryCommand.ExecuteNonQuery();

            conexionCerrar();

           if (filasAfectadas > 0)
           {
               return true;
           }
           else
           {
               return false;
           }
        }

        public void llenarDgv(System.Windows.Forms.DataGridView dgvLlenar, string columnas, string tabla, string condicion, string orderBy) 
        {
            obtenerDatos = new DataSet();
            obtenerDatos.Reset();
        
            adaptador = consultaAdaptador("SELECT " + columnas + " FROM " + tabla + " WHERE " + condicion + " ORDER BY " + orderBy + " ASC");
            adaptador.Fill(obtenerDatos, tabla);

            dgvLlenar.DataSource = obtenerDatos.Tables[0];
        }

        public void cargarImagen(int idVisitante, PictureBox pbImagenRostro, PictureBox pbImagenCredencial)
        {
            string sqlConsulta = "SELECT `fotoRostro`,`fotoCredencial` FROM `Imagen` WHERE `idVisitante` = '" + idVisitante + "'";


            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            adaptador = consultaAdaptador(sqlConsulta);
            adaptador.Fill(obtenerDatos, "Imagen");

            string rutaImagenRostro = obtenerDatos.Tables["Imagen"].Rows[0]["fotoRostro"].ToString();
            string rutaImagenCredencial = obtenerDatos.Tables["Imagen"].Rows[0]["fotoCredencial"].ToString();

            // MessageBox.Show(rutaImagenCredencial+rutaImagenRostro);

            pbImagenRostro.Image = Image.FromFile(rutaImagenRostro);
            pbImagenCredencial.Image = Image.FromFile(rutaImagenCredencial);

        }

        public Notificacion[] manejoAlertas(ToolStripButton tsbNotificacion)
        {
            try
            {

                conexionAbrir();
            
                string sqlConsulta = "SELECT count(*) as Contador FROM Registro INNER JOIN Visitante on Registro.idVisitante=Visitante.idvisitante WHERE Visitante.status='1' AND Registro.fechaIngreso='" + DateTime.Today.ToString("dd/MM/yyy") + "'";
                   
                sqliteCMD(sqlConsulta);
                Reader.Read();
            
                // MessageBox.Show(sqlConsulta);

                int numeroIntenciones = int.Parse(Reader[0].ToString());

                //MessageBox.Show(Environment.MachineName);
            
                conexionCerrar();

            
                if (numeroIntenciones > 0)
                {

                
                    tsbNotificacion.Text = numeroIntenciones + "\nAlertas de Intencion";
                    conexionAbrir();
                    sqlConsulta = "SELECT Registro.idIngreso FROM Registro INNER JOIN Visitante on Registro.idVisitante=Visitante.idvisitante WHERE Visitante.status='1' AND Registro.fechaIngreso='" + DateTime.Today.ToString("dd/MM/yyy") + "'";
                   // MessageBox.Show("numero de intenciones "+numeroIntenciones);
                    int[] idRegistro = new int[numeroIntenciones];
                    sqliteCMD(sqlConsulta);
                
                    int increment = 0;
                    //MessageBox.Show("fieldCount "+reader.FieldCount);
                    while (Reader.Read())
                    {
                      //  MessageBox.Show(reader[0].ToString());
                        idRegistro[increment] = int.Parse(reader[0].ToString());
                        increment++;
                    }
                
                    conexionCerrar();
                    Notificacion[] notificaciones = new Notificacion[numeroIntenciones];
                    Registro registroIntencion;
                    Notificacion notificacionInterna;
                    
                    for (int i = 0; i < numeroIntenciones; i++)
                    {

                        notificacionInterna = new Notificacion();
                        notificacionInterna.Accionador = Environment.MachineName;
                        notificacionInterna.Descripcion = "Visitantes Fuera De Tiempo";
                        notificacionInterna.Fecha = DateTime.Today;
                        notificacionInterna.Hora = DateTime.Now;

                        registroIntencion = new Registro();
                        
                        registroIntencion.cargaRegistroVisitante(idRegistro[i]);

                        notificacionInterna.VisitanteDetalles = registroIntencion;

                        notificaciones[i] = notificacionInterna;

                    }
                
                    return notificaciones;
            
                }
                else
                {
                
                    return null;
            
                }
            
            }catch(Exception ex){
               MessageBox.Show(""+ex);
                return null;
            }
        }
        public SQLiteDataReader Reader
        {
            get { return reader; }
            set { reader = value; }
        }

    }
}

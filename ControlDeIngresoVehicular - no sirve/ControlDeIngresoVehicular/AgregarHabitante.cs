﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlDeIngresoVehicular
{
    public partial class AgregarHabitante : Form
    {
        BD bd = new BD();
        public int entra = 0;
        DataSet obtenerDatos;
        string calle = "";
        string telefono = "";
        string sqlNumeroCasa = "SELECT `numero` FROM `Casa` ORDER BY `numero` ASC";

        public AgregarHabitante()
        {
            InitializeComponent();
            
            bd.llenarComboBox(cbNumero,sqlNumeroCasa, "numero", "Casa", ref entra);
        }

        private void cmdAgregarCasa_Click(object sender, EventArgs e)
        {
            if (cbNumero.Text != "" && txtCalle.Text != "" && txtTelefono.Text != "")
            {
                int numero = Convert.ToInt32(cbNumero.Text);
                string calle = txtCalle.Text;
                string telefono = txtTelefono.Text;

                bool respuesta = bd.insertar("INSERT INTO `Casa`(`numero`,`calle`,`telefono`) VALUES (" + numero + ",'" + calle + "','" + telefono + "')");

                if (respuesta)
                {
                    MessageBox.Show("Datos casa insertados correctamente");
                }
                else
                {
                    MessageBox.Show("Error al intetar insertar datos");
                }
                cmdAgregarCasa.Enabled = false;
                cmdAgregarHabitante.Enabled = true;
            }
            else
            {
                MessageBox.Show("Favor de llenar todos los datos de la casa");
            }
        }

        private void cmdAgregarHabitante_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text != "")
            {
                string nombre = txtNombre.Text;
                
                obtenerDatos = new DataSet();
                obtenerDatos.Reset();

                //Con esta consulta podremos saber si el nombre que se quiere insertar y está en la BD.
                bd.adaptador = bd.consultaAdaptador("SELECT Count(nombre) AS Duplica FROM Colono WHERE nombre = '" + nombre + "'");
                bd.adaptador.Fill(obtenerDatos, "Colono");

                string contadorRegistroDuplicado = obtenerDatos.Tables["Colono"].Rows[0]["Duplica"].ToString();

                int consultaCount = Convert.ToInt32(contadorRegistroDuplicado);
                //Si el contador tiene como resultado mayor a 1, no podrá ser insertado
                if(consultaCount>1)
                {
                    MessageBox.Show("Esta habitante ya está registrado");
                }
                else
                {
                    //Aquí iría el código para insertar
                    int numero = Convert.ToInt32(cbNumero.Text);

                    bool respuesta = bd.insertar("INSERT INTO `Colono`(`nombre`,`numero`) VALUES ('" + nombre + "'," + numero + ")");

                    if (respuesta)
                    {
                        MessageBox.Show("Datos habitante insertados correctamente");
                    }
                    else
                    {
                        MessageBox.Show("Error al intetar insertar datos");
                    }

                    txtNombre.Text = "";
                }

            }
        }

        private void nuevoRegistroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cbNumero.Text = "";
            txtCalle.Text = "";
            txtTelefono.Text = "";
            txtNombre.Text = "";

            cmdModificarCasa.Visible = false;
            cmdModificarHabitante.Visible = false;
            cmdTerminar.Visible = false;

            cmdAgregarCasa.Enabled = true;
            cmdAgregarCasa.Visible= true;

            cmdAgregarHabitante.Enabled = false;

            dgvHabitantes.Visible = false;
            gbDatosHabitante.Visible = true;

            bd.llenarComboBox(cbNumero,sqlNumeroCasa, "numero", "Casa", ref entra);
        }

        private void cbNumero_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (entra != 1)
            {
                cmdAgregarCasa.Visible = false;
                cmdAgregarHabitante.Enabled = true;

                string numero = cbNumero.Text;
                string calle = "";
                string telefono = "";
                string sqlConsulta = "SELECT calle, telefono FROM Casa WHERE numero = " + numero + " ORDER BY numero ASC";

                obtenerDatos = new DataSet();
                obtenerDatos.Reset();

                bd.adaptador = bd.consultaAdaptador(sqlConsulta);
                bd.adaptador.Fill(obtenerDatos, "Casa");

                calle = obtenerDatos.Tables["Casa"].Rows[0]["calle"].ToString();
                telefono = obtenerDatos.Tables["Casa"].Rows[0]["telefono"].ToString();

                txtCalle.Text = calle;
                txtTelefono.Text = telefono;

                this.calle = calle;
                this.telefono = telefono;

                cmdModificarCasa.Visible = true;
                cmdModificarHabitante.Visible = true;
            }
        }

        private void cmdModificarCasa_Click(object sender, EventArgs e)
        {
            string calle = txtCalle.Text;
            string telefono = txtTelefono.Text;
            int numero = Convert.ToInt32(cbNumero.Text);

            if (this.calle != txtCalle.Text || this.telefono != txtTelefono.Text)
            {
                bool respuesta = bd.insertar("UPDATE `Casa` SET `calle`='" + calle + "' ,`telefono`='" + telefono + "'  WHERE `numero`= " + numero + "");

                if (respuesta)
                {
                    MessageBox.Show("Datos de la casa modificados");
                }
                else
                {
                    MessageBox.Show("Error al intetar modificar datos");
                }
            }
            else 
            {
                MessageBox.Show("No hay modificaciones por aplicar");
            }
        }
        private void cmdModificarHabitante_Click(object sender, EventArgs e)
        {
            int numero = Convert.ToInt32(cbNumero.Text);

            bd.llenarDgv(dgvHabitantes, "`nombre`", "`Colono`", "`numero` = " + numero + "", "idcolono");

            dgvHabitantes.Visible = true;
            gbDatosHabitante.Visible = false;
            cmdTerminar.Visible = true;
        }

        private void dgvHabitantes_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            string nombre = dgvHabitantes[e.ColumnIndex, e.RowIndex].Value.ToString();
            // int numero = Convert.ToInt32(cbNumero.Text);
            int numero = Convert.ToInt32(dgvHabitantes.CurrentRow.Index.ToString()) + 1;
            MessageBox.Show(Convert.ToString(numero));
            bool respuesta = bd.insertar("UPDATE `Colono` SET `nombre`='" + nombre + "' WHERE `idcolono`= " + numero + "");

            if (!respuesta)
            {
                MessageBox.Show("Error al intentar modificar");
            }
            //else
            //{
            //    MessageBox.Show("Error al intetar modificar datos");
            //}
        }

        private void cmdTerminar_Click(object sender, EventArgs e)
        {
            dgvHabitantes.Visible = false;
            cmdTerminar.Visible = false;
            gbDatosHabitante.Visible = true;
        }

        private void AgregarHabitante_Load(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Management;

namespace ControlDeIngresoVehicular
{
    public partial class InicioSesion : Form
    {
        BD bd = new BD();
        DataSet obtenerDatos;
        int serial = 0;

        public InicioSesion()
        {
            InitializeComponent();
        }

        private void frmInicioSesion_Load_1(object sender, EventArgs e)
        {
            int status = obtenerStatus();

            if (status == 0)
            {
                abrirActivacion();
                //Activacion activacion = new Activacion();

                //activacion.Show();
                //this.Enabled = false;
                //this.WindowState = FormWindowState.Minimized;
            }
            else if (status == 1) 
            {
                int deGenaraSerial = generarSerial();
                int deObtenerSerial = obtenerNumeroActivacion();

                if (deGenaraSerial != deObtenerSerial)
                {
                    MessageBox.Show("PC no authorizada para el uso de este producto");
                    this.WindowState = FormWindowState.Minimized;
                    Application.Exit();
                    //abrirActivacion();
                }
            }
        }

        private int generarSerial() 
        {
            string hexadecimal = this.codHex();
            serial = int.Parse(hexadecimal, System.Globalization.NumberStyles.HexNumber);
            serial = Math.Abs(serial);

            return serial;
        }

        private void abrirActivacion() 
        {
            Activacion activacion = new Activacion();

            activacion.Show();
            this.Enabled = false;
            this.WindowState = FormWindowState.Minimized;
        }

        private int obtenerStatus()
        {
            int status = 0;
            string sqlConsulta = "SELECT `status` from `Activacion`";

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(sqlConsulta);
            bd.adaptador.Fill(obtenerDatos, "Activacion");

            string recibeDato = obtenerDatos.Tables[0].Rows[0][0].ToString();

            status = Convert.ToInt32(recibeDato);
            return status;
        }

        private int obtenerNumeroActivacion()
        {
            int numActivacion = 0;
            string sqlConsulta = "SELECT `numeroActivacion` from `Activacion`";

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(sqlConsulta);
            bd.adaptador.Fill(obtenerDatos, "Activacion");

            string recibeDato = obtenerDatos.Tables[0].Rows[0][0].ToString();

            numActivacion = Convert.ToInt32(recibeDato);
            return numActivacion;
        }

        public string codHex()
        {
            string HDD = System.Environment.CurrentDirectory.Substring(0, 1);

            ManagementObject disk = new ManagementObject("win32_logicaldisk.deviceid=\"" + HDD + ":\"");

            disk.Get();

            return disk["VolumeSerialNumber"].ToString();
        }

        private void cmdIniciarSesion_Click(object sender, EventArgs e)
        {
            string usuario = txtUsuario.Text.ToUpper();
            string contraseña = txtContraseña.Text.ToLower();
            string contraseñaBase = "";
            string idGuardia = "";
            string permisos = "";
            string nombreUsuario = "";
            //string turno = "";

            //string sqlConsulta = "SELECT u.idGuardia, u.tipo, u.nombreUsuario FROM Usuario AS u, Guardia AS g WHERE nombreUsuario = '" + usuario + "' AND contraseña = '" + contraseña + "' AND g.idGuardia = (SELECT idGuardia FROM Usuario WHERE nombreUsuario = '" + usuario + "' AND contraseña = '" + contraseña + "')";
                
            //"SELECT idGuardia,tipo FROM Usuario WHERE nombreUsuario = '" + usuario + "' AND contraseña = '" + contraseña + "'";

            string sqlConsulta = "SELECT `idGuardia`, `tipo`, `nombreUsuario`, `contraseña` FROM  `Usuario` WHERE `nombreUsuario` = '" + usuario + "' AND `contraseña` = '" + contraseña + "'";

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(sqlConsulta);
            bd.adaptador.Fill(obtenerDatos, "Usuario");

            try
            {
                idGuardia = obtenerDatos.Tables["Usuario"].Rows[0]["idGuardia"].ToString();
                permisos = obtenerDatos.Tables["Usuario"].Rows[0]["tipo"].ToString();
                nombreUsuario = obtenerDatos.Tables["Usuario"].Rows[0]["nombreUsuario"].ToString();
                contraseñaBase = obtenerDatos.Tables["Usuario"].Rows[0]["contraseña"].ToString();
                //turno = obtenerDatos.Tables["Usuario"].Rows[0]["turno"].ToString();

                frmRegistroIngreso registrarIngreso = new frmRegistroIngreso(idGuardia, permisos,nombreUsuario);

                registrarIngreso.Show();
                this.Hide();
            }
            catch (Exception)
            {
                MessageBox.Show("Datos incorrectos favor de verificar");
            }
        }

        private void tester_Click(object sender, EventArgs e)
        {
           
        }
    }
}

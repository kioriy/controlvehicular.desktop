﻿namespace ControlDeIngresoVehicular
{
    partial class ConsultarRegistros
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.dgvConsultaRegistro = new System.Windows.Forms.DataGridView();
            this.cmdConsulta = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbNombreGuardia = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbVisitante = new System.Windows.Forms.ComboBox();
            this.cmdMostrarTodo = new System.Windows.Forms.Button();
            this.chkConFecha = new System.Windows.Forms.CheckBox();
            this.pbImagenRostro = new System.Windows.Forms.PictureBox();
            this.pbImagenCredencial = new System.Windows.Forms.PictureBox();
            this.guardarImg = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsultaRegistro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagenRostro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagenCredencial)).BeginInit();
            this.SuspendLayout();
            // 
            // dtpFecha
            // 
            this.dtpFecha.Location = new System.Drawing.Point(34, 40);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(195, 20);
            this.dtpFecha.TabIndex = 0;
            // 
            // dgvConsultaRegistro
            // 
            this.dgvConsultaRegistro.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvConsultaRegistro.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvConsultaRegistro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvConsultaRegistro.Location = new System.Drawing.Point(12, 78);
            this.dgvConsultaRegistro.Name = "dgvConsultaRegistro";
            this.dgvConsultaRegistro.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvConsultaRegistro.Size = new System.Drawing.Size(796, 440);
            this.dgvConsultaRegistro.TabIndex = 1;
            this.dgvConsultaRegistro.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvConsultaRegistro_CellClick);
            // 
            // cmdConsulta
            // 
            this.cmdConsulta.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdConsulta.ForeColor = System.Drawing.Color.Blue;
            this.cmdConsulta.Location = new System.Drawing.Point(734, 40);
            this.cmdConsulta.Name = "cmdConsulta";
            this.cmdConsulta.Size = new System.Drawing.Size(74, 37);
            this.cmdConsulta.TabIndex = 2;
            this.cmdConsulta.Text = "Realizar consulta";
            this.cmdConsulta.UseVisualStyleBackColor = true;
            this.cmdConsulta.Click += new System.EventHandler(this.cmdConsulta_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(99, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "FECHA";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(277, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 20);
            this.label5.TabIndex = 9;
            this.label5.Text = "GUARDIA";
            // 
            // cbNombreGuardia
            // 
            this.cbNombreGuardia.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbNombreGuardia.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbNombreGuardia.FormattingEnabled = true;
            this.cbNombreGuardia.Location = new System.Drawing.Point(250, 40);
            this.cbNombreGuardia.Name = "cbNombreGuardia";
            this.cbNombreGuardia.Size = new System.Drawing.Size(139, 21);
            this.cbNombreGuardia.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(525, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 20);
            this.label8.TabIndex = 13;
            this.label8.Text = "VISITANTE";
            // 
            // cbVisitante
            // 
            this.cbVisitante.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbVisitante.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbVisitante.FormattingEnabled = true;
            this.cbVisitante.Location = new System.Drawing.Point(415, 40);
            this.cbVisitante.Name = "cbVisitante";
            this.cbVisitante.Size = new System.Drawing.Size(313, 21);
            this.cbVisitante.TabIndex = 14;
            // 
            // cmdMostrarTodo
            // 
            this.cmdMostrarTodo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdMostrarTodo.Location = new System.Drawing.Point(734, 2);
            this.cmdMostrarTodo.Name = "cmdMostrarTodo";
            this.cmdMostrarTodo.Size = new System.Drawing.Size(74, 37);
            this.cmdMostrarTodo.TabIndex = 15;
            this.cmdMostrarTodo.Text = "Mostrar todo";
            this.cmdMostrarTodo.UseVisualStyleBackColor = true;
            this.cmdMostrarTodo.Click += new System.EventHandler(this.cmdMostrarTodo_Click);
            // 
            // chkConFecha
            // 
            this.chkConFecha.AutoSize = true;
            this.chkConFecha.Location = new System.Drawing.Point(14, 43);
            this.chkConFecha.Name = "chkConFecha";
            this.chkConFecha.Size = new System.Drawing.Size(15, 14);
            this.chkConFecha.TabIndex = 16;
            this.chkConFecha.UseVisualStyleBackColor = true;
            // 
            // pbImagenRostro
            // 
            this.pbImagenRostro.Enabled = false;
            this.pbImagenRostro.Location = new System.Drawing.Point(814, 78);
            this.pbImagenRostro.Name = "pbImagenRostro";
            this.pbImagenRostro.Size = new System.Drawing.Size(426, 263);
            this.pbImagenRostro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbImagenRostro.TabIndex = 17;
            this.pbImagenRostro.TabStop = false;
            // 
            // pbImagenCredencial
            // 
            this.pbImagenCredencial.Enabled = false;
            this.pbImagenCredencial.Location = new System.Drawing.Point(865, 347);
            this.pbImagenCredencial.Name = "pbImagenCredencial";
            this.pbImagenCredencial.Size = new System.Drawing.Size(325, 171);
            this.pbImagenCredencial.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbImagenCredencial.TabIndex = 18;
            this.pbImagenCredencial.TabStop = false;
            // 
            // guardarImg
            // 
            this.guardarImg.Location = new System.Drawing.Point(865, 37);
            this.guardarImg.Name = "guardarImg";
            this.guardarImg.Size = new System.Drawing.Size(108, 23);
            this.guardarImg.TabIndex = 19;
            this.guardarImg.Text = "Guardar ";
            this.guardarImg.UseVisualStyleBackColor = true;
            this.guardarImg.Click += new System.EventHandler(this.guardarImagen_Click);
            // 
            // ConsultarRegistros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1252, 531);
            this.Controls.Add(this.guardarImg);
            this.Controls.Add(this.pbImagenCredencial);
            this.Controls.Add(this.pbImagenRostro);
            this.Controls.Add(this.chkConFecha);
            this.Controls.Add(this.cmdMostrarTodo);
            this.Controls.Add(this.cbVisitante);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cbNombreGuardia);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmdConsulta);
            this.Controls.Add(this.dgvConsultaRegistro);
            this.Controls.Add(this.dtpFecha);
            this.Name = "ConsultarRegistros";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ConsultarRegistros";
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsultaRegistro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagenRostro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagenCredencial)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.DataGridView dgvConsultaRegistro;
        private System.Windows.Forms.Button cmdConsulta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbNombreGuardia;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbVisitante;
        private System.Windows.Forms.Button cmdMostrarTodo;
        private System.Windows.Forms.CheckBox chkConFecha;
        public System.Windows.Forms.PictureBox pbImagenRostro;
        private System.Windows.Forms.PictureBox pbImagenCredencial;
        private System.Windows.Forms.Button guardarImg;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Management;

namespace Serializacion
{
    public partial class Form1 : Form
    {
        int serial = 0;
        string hexadecimal = "";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            hexadecimal = codHex();
            serial = int.Parse(hexadecimal, System.Globalization.NumberStyles.HexNumber);
            serial = Math.Abs(serial);

            txtSerie.Text = Convert.ToString(serial);
        }

        public string codHex()
        {
            string HDD = System.Environment.CurrentDirectory.Substring(0, 1);

            ManagementObject disk = new ManagementObject("win32_logicaldisk.deviceid=\"" + HDD + ":\"");

            disk.Get();

            return disk["VolumeSerialNumber"].ToString();
        }
    }
}
